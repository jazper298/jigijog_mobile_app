package com.example.jigijog_mobile_app.interfaces;

import android.view.View;

public interface OnClickRecyclerView {

        void onItemClick(View view, int position);
}
