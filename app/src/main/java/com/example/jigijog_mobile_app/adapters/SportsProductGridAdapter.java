package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Sports;
import com.example.jigijog_mobile_app.models.Women;

import java.util.ArrayList;

public class SportsProductGridAdapter extends RecyclerView.Adapter<SportsProductGridAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Sports> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public SportsProductGridAdapter(Context context, ArrayList<Sports> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_sports_products_grid, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Sports sportsModel = mList.get(position);
        ImageView imageView = holder.iv_sportsImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_sportsPrice;
        textView2 = holder.tv_sportsDisPrice;
        textView3 = holder.tv_sportsItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + sportsModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_sportsImage);
        double unit_price =  Double.parseDouble(sportsModel.getUnit_price());
        double discount;
        if (sportsModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(sportsModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(sportsModel.getDiscount());
        if (sportsModel.getName().length() <= 24){
            textView3.setText(sportsModel.getName());
        }else{
            textView3.setText(sportsModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_sportsImage;
        TextView tv_sportsPrice, tv_sportsDisPrice, tv_sportsItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_sportsImage = (ImageView) itemView.findViewById(R.id.iv_sportsImage);
            tv_sportsPrice = (TextView) itemView.findViewById(R.id.tv_sportsPrice);
            tv_sportsDisPrice = (TextView) itemView.findViewById(R.id.tv_sportsDisPrice);
            tv_sportsItemName = (TextView) itemView.findViewById(R.id.tv_sportsItemName);

            iv_sportsImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
