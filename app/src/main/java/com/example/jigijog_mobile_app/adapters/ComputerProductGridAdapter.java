package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Computer;
import com.example.jigijog_mobile_app.models.Women;

import java.util.ArrayList;

public class ComputerProductGridAdapter extends RecyclerView.Adapter<ComputerProductGridAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Computer> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public ComputerProductGridAdapter(Context context, ArrayList<Computer> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_computer_products_grid, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Computer computerModel = mList.get(position);
        ImageView imageView = holder.iv_computerImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_computerPrice;
        textView2 = holder.tv_computerDisPrice;
        textView3 = holder.tv_computerItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + computerModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_computerImage);
        double unit_price =  Double.parseDouble(computerModel.getUnit_price());
        double discount;
        if (computerModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(computerModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(computerModel.getDiscount());
        if (computerModel.getName().length() <= 24){
            textView3.setText(computerModel.getName());
        }else{
            textView3.setText(computerModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_computerImage;
        TextView tv_computerPrice, tv_computerDisPrice, tv_computerItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_computerImage = (ImageView) itemView.findViewById(R.id.iv_computerImage);
            tv_computerPrice = (TextView) itemView.findViewById(R.id.tv_computerPrice);
            tv_computerDisPrice = (TextView) itemView.findViewById(R.id.tv_computerDisPrice);
            tv_computerItemName = (TextView) itemView.findViewById(R.id.tv_computerItemName);

            iv_computerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
