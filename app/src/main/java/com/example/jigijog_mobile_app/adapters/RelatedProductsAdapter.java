package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Assorted;
import com.example.jigijog_mobile_app.models.RelatedProducts;

import java.util.List;

public class RelatedProductsAdapter extends RecyclerView.Adapter<RelatedProductsAdapter.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROGRESS = 0;

    private int item_per_display = 0;

    private View view;
    private Context mContext;
    private List<RelatedProducts> mList;
    private OnClickRecyclerView onClickRecyclerView;
    private boolean loading;
    private AssordtedGridAdapter.OnLoadMoreListener onLoadMoreListener = null;
    private AssortedAdapter.OnItemClickListener mOnItemClickListener;
    public interface OnItemClickListener {
        void onItemClick(View view, Assorted obj, int position);
    }
    public void setOnItemClickListener(final AssortedAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }
    public RelatedProductsAdapter(Context context,List<RelatedProducts> list) {
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        view = layoutInflater.inflate(R.layout.listrow_assorted_products,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final RelatedProducts relatedProducts = mList.get(position);
        ImageView imageView = holder.iv_assortedImage;
        TextView textView1, textView2,textView3;

        textView1 = holder.tv_assortedPrice;
        textView2 = holder.tv_assortedDisPrice;
        textView3 = holder.tv_assortedItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + relatedProducts.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_assortedImage);
        double unit_price =  Double.parseDouble(relatedProducts.getUnit_price());
        double discount;
        if (relatedProducts.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(relatedProducts.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " +String.valueOf(total) + "0");
        textView2.setText("\u20B1" + " " + relatedProducts.getDiscount());
        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (relatedProducts.getName().length() <= 24){
            textView3.setText(relatedProducts.getName());
        }else{
            textView3.setText(relatedProducts.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_assortedImage;
        TextView tv_assortedPrice,tv_assortedDisPrice, tv_assortedItemName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_assortedImage = (ImageView) itemView.findViewById(R.id.iv_assortedImage);
            tv_assortedPrice = (TextView) itemView.findViewById(R.id.tv_assortedPrice);
            tv_assortedDisPrice = (TextView) itemView.findViewById(R.id.tv_assortedDisPrice);
            tv_assortedItemName = (TextView) itemView.findViewById(R.id.tv_assortedItemName);

            iv_assortedImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
