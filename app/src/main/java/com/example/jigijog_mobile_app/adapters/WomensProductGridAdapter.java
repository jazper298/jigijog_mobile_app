package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Men;
import com.example.jigijog_mobile_app.models.Women;

import java.util.ArrayList;

public class WomensProductGridAdapter extends RecyclerView.Adapter<WomensProductGridAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Women> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public WomensProductGridAdapter(Context context, ArrayList<Women> list) {
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public WomensProductGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_womens_products_grid, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WomensProductGridAdapter.ViewHolder holder, int position) {
        final Women womenModel = mList.get(position);
        ImageView imageView = holder.iv_womensImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_womensPrice;
        textView2 = holder.tv_womensDisPrice;
        textView3 = holder.tv_womensItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + womenModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_womensImage);
        double unit_price =  Double.parseDouble(womenModel.getUnit_price());
        double discount;
        if (womenModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(womenModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(womenModel.getDiscount());
        if (womenModel.getName().length() <= 24){
            textView3.setText(womenModel.getName());
        }else{
            textView3.setText(womenModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_womensImage;
        TextView tv_womensPrice, tv_womensDisPrice, tv_womensItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_womensImage = (ImageView) itemView.findViewById(R.id.iv_womensImage);
            tv_womensPrice = (TextView) itemView.findViewById(R.id.tv_womensPrice);
            tv_womensDisPrice = (TextView) itemView.findViewById(R.id.tv_womensDisPrice);
            tv_womensItemName = (TextView) itemView.findViewById(R.id.tv_womensItemName);

            iv_womensImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
