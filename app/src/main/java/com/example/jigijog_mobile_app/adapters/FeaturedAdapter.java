package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Featured;

import java.util.ArrayList;

public class FeaturedAdapter extends RecyclerView.Adapter<FeaturedAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Featured> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public FeaturedAdapter(Context context, ArrayList<Featured> list) {
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_featured_product,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final Featured featuredMddel = mList.get(position);
        ImageView imageView = holder.iv_featuredImage;
        TextView textView1, textView2,textView3;

        textView1 = holder.tv_featuredPrice;
        textView2 = holder.tv_featuredPrice2;
        textView3 = holder.tv_featuredItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + featuredMddel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_featuredImage);
        double unit_price =  Double.parseDouble(featuredMddel.getUnit_price());
        double discount =  Double.parseDouble(featuredMddel.getDiscount());
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " +String.valueOf(total) + "0");
        textView2.setText("\u20B1" + " " + featuredMddel.getDiscount());
        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (featuredMddel.getName().length() <= 24){
            textView3.setText(featuredMddel.getName());
        }else{
            textView3.setText(featuredMddel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_featuredImage;
        TextView tv_featuredPrice,tv_featuredPrice2, tv_featuredItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_featuredImage = (ImageView) itemView.findViewById(R.id.iv_featuredImage);
            tv_featuredPrice = (TextView) itemView.findViewById(R.id.tv_featuredPrice);
            tv_featuredPrice2 = (TextView) itemView.findViewById(R.id.tv_featuredPrice2);
            tv_featuredItemName = (TextView) itemView.findViewById(R.id.tv_featuredItemName);
            iv_featuredImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
