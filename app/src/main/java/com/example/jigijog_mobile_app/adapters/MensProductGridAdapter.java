package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Men;

import java.util.ArrayList;

public class MensProductGridAdapter extends RecyclerView.Adapter<MensProductGridAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Men> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public MensProductGridAdapter(Context context, ArrayList<Men> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_mens_products_grid, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Men menModel = mList.get(position);
        ImageView imageView = holder.iv_mensImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_mensPrice;
        textView2 = holder.tv_mensDisPrice;
        textView3 = holder.tv_mensItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + menModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_mensImage);
        double unit_price =  Double.parseDouble(menModel.getUnit_price());
        double discount =  Double.parseDouble(menModel.getDiscount());
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(menModel.getDiscount());
        if (menModel.getName().length() <= 24){
            textView3.setText(menModel.getName());
        }else{
            textView3.setText(menModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_mensImage;
        TextView tv_mensPrice, tv_mensDisPrice, tv_mensItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_mensImage = (ImageView) itemView.findViewById(R.id.iv_mensImage);
            tv_mensPrice = (TextView) itemView.findViewById(R.id.tv_mensPrice);
            tv_mensDisPrice = (TextView) itemView.findViewById(R.id.tv_mensDisPrice);
            tv_mensItemName = (TextView) itemView.findViewById(R.id.tv_mensItemName);

            iv_mensImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
