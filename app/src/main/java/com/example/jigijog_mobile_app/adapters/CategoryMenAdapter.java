package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.CategoryMen;

import java.util.ArrayList;

public class CategoryMenAdapter extends RecyclerView.Adapter<CategoryMenAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<CategoryMen> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public CategoryMenAdapter(Context context, ArrayList<CategoryMen> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_category_men, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CategoryMen categoryMenModel = mList.get(position);
        ImageView imageView = holder.iv_categoryMenImage;
        TextView tv_categoryName;

        tv_categoryName = holder.tv_categoryMenName;
        imageView.setImageResource(categoryMenModel.getCategoryImage());

        tv_categoryName.setText(categoryMenModel.getCategoryName());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_categoryMenImage;
        TextView tv_categoryMenName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_categoryMenImage = (ImageView) itemView.findViewById(R.id.iv_categoryMenImage);
            tv_categoryMenName = (TextView) itemView.findViewById(R.id.tv_categoryMenName);

            iv_categoryMenImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }

    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}