package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Computer;
import com.example.jigijog_mobile_app.models.Jewelry;

import java.util.ArrayList;

public class JewelryProductAdapter extends RecyclerView.Adapter<JewelryProductAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Jewelry> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public JewelryProductAdapter(Context context, ArrayList<Jewelry> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_jewelry_products, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Jewelry jewelryModel = mList.get(position);
        ImageView imageView = holder.iv_jewelryImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_jewelryPrice;
        textView2 = holder.tv_jewelryDisPrice;
        textView3 = holder.tv_jewelryItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + jewelryModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_jewelryImage);
        double unit_price =  Double.parseDouble(jewelryModel.getUnit_price());
        double discount;
        if (jewelryModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(jewelryModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(jewelryModel.getDiscount());
        if (jewelryModel.getName().length() <= 24){
            textView3.setText(jewelryModel.getName());
        }else{
            textView3.setText(jewelryModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_jewelryImage;
        TextView tv_jewelryPrice, tv_jewelryDisPrice, tv_jewelryItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_jewelryImage = (ImageView) itemView.findViewById(R.id.iv_jewelryImage);
            tv_jewelryPrice = (TextView) itemView.findViewById(R.id.tv_jewelryPrice);
            tv_jewelryDisPrice = (TextView) itemView.findViewById(R.id.tv_jewelryDisPrice);
            tv_jewelryItemName = (TextView) itemView.findViewById(R.id.tv_jewelryItemName);

            iv_jewelryImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
