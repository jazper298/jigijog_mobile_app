package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Featured;
import com.example.jigijog_mobile_app.models.Hardware;

import java.util.ArrayList;

public class HardwareProductsGridAdapter extends RecyclerView.Adapter<HardwareProductsGridAdapter.ViewHolder>{
    private Context mContext;
    private ArrayList<Hardware> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public HardwareProductsGridAdapter(Context context, ArrayList<Hardware> list) {
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public HardwareProductsGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_featured_products_grid, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HardwareProductsGridAdapter.ViewHolder holder, int position) {
        final Hardware hardwareModel = mList.get(position);
        ImageView imageView = holder.iv_featuredImage;
        TextView textView1, textView2,textView3;

        textView1 = holder.tv_featuredPrice;
        textView2 = holder.tv_featuredDisPrice;
        textView3 = holder.tv_featuredItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + hardwareModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_featuredImage);
        double unit_price =  Double.parseDouble(hardwareModel.getUnit_price());
        double discount;
        if (hardwareModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(hardwareModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(hardwareModel.getDiscount());
        if (hardwareModel.getName().length() <= 24){
            textView3.setText(hardwareModel.getName());
        }else{
            textView3.setText(hardwareModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_featuredImage;
        TextView tv_featuredPrice,tv_featuredDisPrice, tv_featuredItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_featuredImage = (ImageView) itemView.findViewById(R.id.iv_featuredImage);
            tv_featuredPrice = (TextView) itemView.findViewById(R.id.tv_featuredPrice);
            tv_featuredDisPrice = (TextView) itemView.findViewById(R.id.tv_featuredDisPrice);
            tv_featuredItemName = (TextView) itemView.findViewById(R.id.tv_featuredItemName);
            iv_featuredImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
