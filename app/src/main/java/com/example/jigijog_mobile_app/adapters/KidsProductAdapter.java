package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Jewelry;
import com.example.jigijog_mobile_app.models.Kids;

import java.util.ArrayList;

public class KidsProductAdapter extends RecyclerView.Adapter<KidsProductAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Kids> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public KidsProductAdapter(Context context, ArrayList<Kids> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_kids_products, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Kids kidsModel = mList.get(position);
        ImageView imageView = holder.iv_kidsImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_kidsPrice;
        textView2 = holder.tv_kidsDisPrice;
        textView3 = holder.tv_kidsItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + kidsModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_kidsImage);
        double unit_price =  Double.parseDouble(kidsModel.getUnit_price());
        double discount;
        if (kidsModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(kidsModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(kidsModel.getDiscount());
        if (kidsModel.getName().length() <= 24){
            textView3.setText(kidsModel.getName());
        }else{
            textView3.setText(kidsModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_kidsImage;
        TextView tv_kidsPrice, tv_kidsDisPrice, tv_kidsItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_kidsImage = (ImageView) itemView.findViewById(R.id.iv_kidsImage);
            tv_kidsPrice = (TextView) itemView.findViewById(R.id.tv_kidsPrice);
            tv_kidsDisPrice = (TextView) itemView.findViewById(R.id.tv_kidsDisPrice);
            tv_kidsItemName = (TextView) itemView.findViewById(R.id.tv_kidsItemName);

            iv_kidsImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
