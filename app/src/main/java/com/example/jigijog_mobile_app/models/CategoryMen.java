package com.example.jigijog_mobile_app.models;

public class CategoryMen {
    private String categoryMenName;
    private int categoryMenImage;


    public CategoryMen(String categoryMenName, int categoryMenImage) {
        this.categoryMenName = categoryMenName;
        this.categoryMenImage = categoryMenImage;
    }
    public CategoryMen() {

    }

    public String getCategoryName() {
        return categoryMenName;
    }

    public void setCategoryName(String categoryMenName) {
        this.categoryMenName = categoryMenName;
    }

    public int getCategoryImage() {
        return categoryMenImage;
    }

    public void setCategoryImage(int categoryMenImage) {
        this.categoryMenImage = categoryMenImage;
    }
}
