package com.example.jigijog_mobile_app.models;

public class Flipper {
    private String id;
    private String photos;
    private String created_at;
    private String updated_at;

    public Flipper(String id, String photos, String created_at, String updated_at) {
        this.id = id;
        this.photos = photos;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public Flipper() {
    }

    public String getId() {
        return id;
    }

    public String getPhotos() {
        return photos;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
