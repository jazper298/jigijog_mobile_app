package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jigijog_mobile_app.HttpProvider;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.AssordtedGridAdapter;
import com.example.jigijog_mobile_app.adapters.CartAdapter;
import com.example.jigijog_mobile_app.adapters.CustomSwipeAdapter2;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Assorted;
import com.example.jigijog_mobile_app.models.Cart;
import com.example.jigijog_mobile_app.models.Products;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class CartActivity extends AppCompatActivity {

    private View view;
    private Context context;

    private ImageView iv_mycartBack;
    private TextView tv_mycartName,iv_mycartDelete;
    private Button btn_applyVoucher;

    private String productID;
    private ArrayList<Products> productsArrayList;
    private ArrayList<Products.Photos> photosArrayList;
    private RecyclerView cartRecyclerView;
    private CartAdapter cartAdapter;
    private ArrayList<Cart> cartArrayList;
    private ArrayList<Cart.Photos> cartPhotosArrayList;
    private Cart selectedCart = new Cart();

    private Products selectedProducts = new Products();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Tools.setSystemBarColor(this, R.color.deep_orange_400);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        productID = intent.getStringExtra("PRODUCTID");

        initializeUI();
        loadProductItemByID(Integer.parseInt(productID));
    }

    private void initializeUI() {
        cartRecyclerView = findViewById(R.id.cartRecyclerView);
        iv_mycartBack = findViewById(R.id.iv_mycartBack);
        tv_mycartName = findViewById(R.id.tv_mycartName);
        btn_applyVoucher = findViewById(R.id.btn_applyVoucher);
        iv_mycartDelete = findViewById(R.id.iv_mycartDelete);
        iv_mycartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_applyVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.warning(context, "Button Voucher code Clicked").show();
            }
        });
        iv_mycartDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.warning(context, "Delete Cart Function Clicked").show();
            }
        });
    }
    private void loadProductItemByID(int productID){
        cartArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("product_id", productID);
        Debugger.logD("cart "+ productID);
        HttpProvider.post(context, "/mobile/selectedProductById", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            Cart productsModel = new Cart();

                            cartPhotosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);

                                String photos2 = jobject.getString("photos");

                                String result = photos2.substring(2, photos2.length() -2).replace("\\","");

                                Cart.Photos photos1 = new Cart.Photos();
                                photos1.setPhoto(result);

                                cartPhotosArrayList.add(photos1);

                            }

//                            choice_optionsArrayList = new ArrayList<>();
//                            JSONArray jsonArray2 = new JSONArray(Arrays.asList(choice_options));
//                            for (int k = 0; k <= jsonArray2.length() - 1; k++) {
//                                JSONObject jsonObject2 = jsonArray2.getJSONObject(k);
//                                String choice_options2 = jsonObject2.getString("choice_options");
//
//                                Products.Choice_Options choice_options1 = new Products.Choice_Options();
//                                choice_options1.setChoice_option(choice_options2);
//
//                                choice_optionsArrayList.add(choice_options1);
//                            }
//                            colorsArrayList = new ArrayList<>();
//                            JSONArray jsonArray3 = new JSONArray(Arrays.asList(colors));
//                            for (int l = 0; l <= jsonArray2.length() - 1; l++) {
//                                JSONObject jsonObject2 = jsonArray3.getJSONObject(l);
//                                String colors2 = jsonObject2.getString("colors");
//
//                                Products.Colors colors1 = new Products.Colors();
//                                colors1.setColor(colors2);
//
//                                colorsArrayList.add(colors1);
//                            }
//
//                            variationsArrayList = new ArrayList<>();
//                            JSONArray jsonArray4 = new JSONArray(Arrays.asList(variations));
//                            for (int n = 0; n <= jsonArray2.length() - 1; n++) {
//                                JSONObject jsonObject2 = jsonArray4.getJSONObject(n);
//                                String variations2 = jsonObject2.getString("variations");
//
//                                Products.Variations variations1 = new Products.Variations();
//                                variations1.setVariation(variations2);
//
//                                variationsArrayList.add(variations1);
//                            }

                            productsModel.setId(id);
                            productsModel.setName(name);
                            productsModel.setAdded_by(added_by);
                            productsModel.setUser_id(user_id);
                            productsModel.setCategory_id(category_id);
                            productsModel.setSubcategory_id(subcategory_id);
                            productsModel.setSubsubcategory_id(subsubcategory_id);
                            productsModel.setBrand_id(brand_id);
                            //productsModel.setPhotos(photos);
                            productsModel.setThumbnail_img(thumbnail_img);
                            productsModel.setFeatured_img(featured_img);
                            productsModel.setFlash_deal_img(flash_deal_img);
                            productsModel.setVideo_provider(video_provider);
                            productsModel.setVideo_link(video_link);
                            productsModel.setTags(tags);
                            productsModel.setDescription(description);
                            productsModel.setUnit_price(unit_price);
                            productsModel.setPurchase_price(purchase_price);
                            //productsModel.setChoice_options(choice_options);
                            // productsModel.setColors(colors);
                            //productsModel.setVariations(variations);
                            productsModel.setTodays_deal(todays_deal);
                            productsModel.setPublished(published);
                            productsModel.setFeatured(featured);
                            productsModel.setCurrent_stock(current_stock);
                            productsModel.setUnit(unit);
                            productsModel.setDiscount(discount);
                            productsModel.setDiscount_type(discount_type);
                            productsModel.setTax(tax);
                            productsModel.setTax_type(tax_type);
                            productsModel.setShipping_type(shipping_type);
                            productsModel.setShipping_cost(shipping_cost);
                            productsModel.setWeight(weight);
                            productsModel.setParcel_size(parcel_size);
                            productsModel.setNum_of_sale(num_of_sale);
                            productsModel.setMeta_title(meta_title);
                            productsModel.setMeta_description(meta_description);
                            productsModel.setMeta_img(meta_img);
                            productsModel.setPdf(pdf);
                            productsModel.setSlug(slug);
                            productsModel.setRating(rating);
                            productsModel.setCreated_at(created_at);
                            productsModel.setUpdated_at(updated_at);

                            cartArrayList.add(productsModel);

                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        cartRecyclerView.setLayoutManager(layoutManager);

                        cartAdapter = new CartAdapter(context, cartArrayList);
                        cartAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedCart = cartArrayList.get(position);
                            }
                        });
                        cartRecyclerView.setAdapter(cartAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

}
