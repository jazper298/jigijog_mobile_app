package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.HardwareProductsAdapter;
import com.example.jigijog_mobile_app.adapters.HomeProductsAdapter;
import com.example.jigijog_mobile_app.adapters.HomeProductsGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Hardware;
import com.example.jigijog_mobile_app.models.Home;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class HomeCategoriesActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private ImageView iv_homecategoryBack, iv_homecategoySearch, iv_homecategoryCart, iv_homecategoryNewest, iv_homecategoryGrid,iv_homecategoryMore;
    private TextView tv_homecategoryName;
    private String message;

    private ConstraintLayout emptyIndicator;

    private ArrayList<Home> homeArrayList;
    private RecyclerView homecategoryRecyclerView;
    private HomeProductsAdapter homeProductsAdapter;
    private HomeProductsGridAdapter homeProductsGridAdapter;

    private Home selectedHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();
        if(message.equals("Electrical Appliances")){
            tv_homecategoryName.setText(message);
            loadHomeElectricalAppliances();
            iv_homecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHomeElectricalAppliancesGrid();
                }
            });
        }else if(message.equals("Large Kitchen Appliances")){
            tv_homecategoryName.setText(message);
            loadHOmeKitchenAppliances();
            iv_homecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHOmeKitchenAppliancesGrid();
                }
            });
        }else if(message.equals("Others")){
            tv_homecategoryName.setText(message);
            loadHomeOthers();
            iv_homecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHomeOthersGrid();
                }
            });
        }else if(message.equals("TV")){
            tv_homecategoryName.setText(message);
            loadHomeTv();
            iv_homecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHomeTvGrid();
                }
            });
        }
    }

    private void initializeUI(){
        emptyIndicator = findViewById(R.id.view_Empty);
        iv_homecategoryBack = findViewById(R.id.iv_homecategoryBack);
        iv_homecategoySearch = findViewById(R.id.iv_homecategoySearch);
        iv_homecategoryCart = findViewById(R.id.iv_homecategoryCart);
        iv_homecategoryNewest = findViewById(R.id.iv_homecategoryNewest);
        iv_homecategoryGrid = findViewById(R.id.iv_homecategoryGrid);
        tv_homecategoryName = findViewById(R.id.tv_homecategoryName);
        iv_homecategoryMore = findViewById(R.id.iv_homecategoryMore);
        iv_homecategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_homecategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });


    }

    private void loadHomeElectricalAppliances(){
        homecategoryRecyclerView = findViewById(R.id.homecategoryRecyclerView);
        homeArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllElectricalAppliances", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Home homeModel = new Home();
                            homeModel.setId(id);
                            homeModel.setName(name);
                            homeModel.setAdded_by(added_by);
                            homeModel.setCategory_id(category_id);
                            homeModel.setSubcategory_id(subcategory_id);
                            homeModel.setSubsubcategory_id(subsubcategory_id);
                            homeModel.setBrand_id(brand_id);
                            homeModel.setPhotos(photos);
                            homeModel.setThumbnail_img(thumbnail_img);
                            homeModel.setFeatured_img(featured_img);
                            homeModel.setFlash_deal_img(flash_deal_img);
                            homeModel.setVideo_provider(video_provider);
                            homeModel.setVideo_link(video_link);
                            homeModel.setTags(tags);
                            homeModel.setDescription(description);
                            homeModel.setUnit_price(unit_price);
                            homeModel.setPurchase_price(purchase_price);
                            homeModel.setChoice_options(choice_options);
                            homeModel.setColors(colors);
                            homeModel.setVariations(variations);
                            homeModel.setTodays_deal(todays_deal);
                            homeModel.setPublished(published);
                            homeModel.setFeatured(featured);
                            homeModel.setCurrent_stock(current_stock);
                            homeModel.setUnit(unit);
                            homeModel.setDiscount(discount);
                            homeModel.setDiscount_type(discount_type);
                            homeModel.setTax(tax);
                            homeModel.setTax_type(tax_type);
                            homeModel.setShipping_type(shipping_type);
                            homeModel.setShipping_cost(shipping_cost);
                            homeModel.setWeight(weight);
                            homeModel.setParcel_size(parcel_size);
                            homeModel.setNum_of_sale(num_of_sale);
                            homeModel.setMeta_title(meta_title);
                            homeModel.setMeta_description(meta_description);
                            homeModel.setMeta_img(meta_img);
                            homeModel.setPdf(pdf);
                            homeModel.setSlug(slug);
                            homeModel.setRating(rating);
                            homeModel.setCreated_at(created_at);
                            homeModel.setUpdated_at(updated_at);

                            homeArrayList.add(homeModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        homecategoryRecyclerView.setLayoutManager(layoutManager);

                        homeProductsAdapter = new HomeProductsAdapter(context, homeArrayList);
                        homeProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHome = homeArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHome.getName());
                                intent.putExtra("ITEMPRICE", selectedHome.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHome.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHome.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHome.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        homecategoryRecyclerView.setAdapter(homeProductsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHOmeKitchenAppliances(){
        homecategoryRecyclerView = findViewById(R.id.homecategoryRecyclerView);
        homeArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllLargeKitchenAppliances", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Home homeModel = new Home();
                            homeModel.setId(id);
                            homeModel.setName(name);
                            homeModel.setAdded_by(added_by);
                            homeModel.setCategory_id(category_id);
                            homeModel.setSubcategory_id(subcategory_id);
                            homeModel.setSubsubcategory_id(subsubcategory_id);
                            homeModel.setBrand_id(brand_id);
                            homeModel.setPhotos(photos);
                            homeModel.setThumbnail_img(thumbnail_img);
                            homeModel.setFeatured_img(featured_img);
                            homeModel.setFlash_deal_img(flash_deal_img);
                            homeModel.setVideo_provider(video_provider);
                            homeModel.setVideo_link(video_link);
                            homeModel.setTags(tags);
                            homeModel.setDescription(description);
                            homeModel.setUnit_price(unit_price);
                            homeModel.setPurchase_price(purchase_price);
                            homeModel.setChoice_options(choice_options);
                            homeModel.setColors(colors);
                            homeModel.setVariations(variations);
                            homeModel.setTodays_deal(todays_deal);
                            homeModel.setPublished(published);
                            homeModel.setFeatured(featured);
                            homeModel.setCurrent_stock(current_stock);
                            homeModel.setUnit(unit);
                            homeModel.setDiscount(discount);
                            homeModel.setDiscount_type(discount_type);
                            homeModel.setTax(tax);
                            homeModel.setTax_type(tax_type);
                            homeModel.setShipping_type(shipping_type);
                            homeModel.setShipping_cost(shipping_cost);
                            homeModel.setWeight(weight);
                            homeModel.setParcel_size(parcel_size);
                            homeModel.setNum_of_sale(num_of_sale);
                            homeModel.setMeta_title(meta_title);
                            homeModel.setMeta_description(meta_description);
                            homeModel.setMeta_img(meta_img);
                            homeModel.setPdf(pdf);
                            homeModel.setSlug(slug);
                            homeModel.setRating(rating);
                            homeModel.setCreated_at(created_at);
                            homeModel.setUpdated_at(updated_at);

                            homeArrayList.add(homeModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        homecategoryRecyclerView.setLayoutManager(layoutManager);

                        homeProductsAdapter = new HomeProductsAdapter(context, homeArrayList);
                        homeProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHome = homeArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHome.getName());
                                intent.putExtra("ITEMPRICE", selectedHome.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHome.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHome.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHome.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        homecategoryRecyclerView.setAdapter(homeProductsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHomeOthers(){
        homecategoryRecyclerView = findViewById(R.id.homecategoryRecyclerView);
        homeArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHomeAppliancesOthers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Home homeModel = new Home();
                            homeModel.setId(id);
                            homeModel.setName(name);
                            homeModel.setAdded_by(added_by);
                            homeModel.setCategory_id(category_id);
                            homeModel.setSubcategory_id(subcategory_id);
                            homeModel.setSubsubcategory_id(subsubcategory_id);
                            homeModel.setBrand_id(brand_id);
                            homeModel.setPhotos(photos);
                            homeModel.setThumbnail_img(thumbnail_img);
                            homeModel.setFeatured_img(featured_img);
                            homeModel.setFlash_deal_img(flash_deal_img);
                            homeModel.setVideo_provider(video_provider);
                            homeModel.setVideo_link(video_link);
                            homeModel.setTags(tags);
                            homeModel.setDescription(description);
                            homeModel.setUnit_price(unit_price);
                            homeModel.setPurchase_price(purchase_price);
                            homeModel.setChoice_options(choice_options);
                            homeModel.setColors(colors);
                            homeModel.setVariations(variations);
                            homeModel.setTodays_deal(todays_deal);
                            homeModel.setPublished(published);
                            homeModel.setFeatured(featured);
                            homeModel.setCurrent_stock(current_stock);
                            homeModel.setUnit(unit);
                            homeModel.setDiscount(discount);
                            homeModel.setDiscount_type(discount_type);
                            homeModel.setTax(tax);
                            homeModel.setTax_type(tax_type);
                            homeModel.setShipping_type(shipping_type);
                            homeModel.setShipping_cost(shipping_cost);
                            homeModel.setWeight(weight);
                            homeModel.setParcel_size(parcel_size);
                            homeModel.setNum_of_sale(num_of_sale);
                            homeModel.setMeta_title(meta_title);
                            homeModel.setMeta_description(meta_description);
                            homeModel.setMeta_img(meta_img);
                            homeModel.setPdf(pdf);
                            homeModel.setSlug(slug);
                            homeModel.setRating(rating);
                            homeModel.setCreated_at(created_at);
                            homeModel.setUpdated_at(updated_at);

                            homeArrayList.add(homeModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        homecategoryRecyclerView.setLayoutManager(layoutManager);

                        homeProductsAdapter = new HomeProductsAdapter(context, homeArrayList);
                        homeProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHome = homeArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHome.getName());
                                intent.putExtra("ITEMPRICE", selectedHome.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHome.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHome.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHome.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        homecategoryRecyclerView.setAdapter(homeProductsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHomeTv(){
        homecategoryRecyclerView = findViewById(R.id.homecategoryRecyclerView);
        homeArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHomeAppliancesTV", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Home homeModel = new Home();
                            homeModel.setId(id);
                            homeModel.setName(name);
                            homeModel.setAdded_by(added_by);
                            homeModel.setCategory_id(category_id);
                            homeModel.setSubcategory_id(subcategory_id);
                            homeModel.setSubsubcategory_id(subsubcategory_id);
                            homeModel.setBrand_id(brand_id);
                            homeModel.setPhotos(photos);
                            homeModel.setThumbnail_img(thumbnail_img);
                            homeModel.setFeatured_img(featured_img);
                            homeModel.setFlash_deal_img(flash_deal_img);
                            homeModel.setVideo_provider(video_provider);
                            homeModel.setVideo_link(video_link);
                            homeModel.setTags(tags);
                            homeModel.setDescription(description);
                            homeModel.setUnit_price(unit_price);
                            homeModel.setPurchase_price(purchase_price);
                            homeModel.setChoice_options(choice_options);
                            homeModel.setColors(colors);
                            homeModel.setVariations(variations);
                            homeModel.setTodays_deal(todays_deal);
                            homeModel.setPublished(published);
                            homeModel.setFeatured(featured);
                            homeModel.setCurrent_stock(current_stock);
                            homeModel.setUnit(unit);
                            homeModel.setDiscount(discount);
                            homeModel.setDiscount_type(discount_type);
                            homeModel.setTax(tax);
                            homeModel.setTax_type(tax_type);
                            homeModel.setShipping_type(shipping_type);
                            homeModel.setShipping_cost(shipping_cost);
                            homeModel.setWeight(weight);
                            homeModel.setParcel_size(parcel_size);
                            homeModel.setNum_of_sale(num_of_sale);
                            homeModel.setMeta_title(meta_title);
                            homeModel.setMeta_description(meta_description);
                            homeModel.setMeta_img(meta_img);
                            homeModel.setPdf(pdf);
                            homeModel.setSlug(slug);
                            homeModel.setRating(rating);
                            homeModel.setCreated_at(created_at);
                            homeModel.setUpdated_at(updated_at);

                            homeArrayList.add(homeModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        homecategoryRecyclerView.setLayoutManager(layoutManager);

                        homeProductsAdapter = new HomeProductsAdapter(context, homeArrayList);
                        homeProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHome = homeArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHome.getName());
                                intent.putExtra("ITEMPRICE", selectedHome.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHome.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHome.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHome.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        homecategoryRecyclerView.setAdapter(homeProductsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadHomeElectricalAppliancesGrid(){
        homecategoryRecyclerView = findViewById(R.id.homecategoryRecyclerView);
        homeArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllElectricalAppliances", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Home homeModel = new Home();
                            homeModel.setId(id);
                            homeModel.setName(name);
                            homeModel.setAdded_by(added_by);
                            homeModel.setCategory_id(category_id);
                            homeModel.setSubcategory_id(subcategory_id);
                            homeModel.setSubsubcategory_id(subsubcategory_id);
                            homeModel.setBrand_id(brand_id);
                            homeModel.setPhotos(photos);
                            homeModel.setThumbnail_img(thumbnail_img);
                            homeModel.setFeatured_img(featured_img);
                            homeModel.setFlash_deal_img(flash_deal_img);
                            homeModel.setVideo_provider(video_provider);
                            homeModel.setVideo_link(video_link);
                            homeModel.setTags(tags);
                            homeModel.setDescription(description);
                            homeModel.setUnit_price(unit_price);
                            homeModel.setPurchase_price(purchase_price);
                            homeModel.setChoice_options(choice_options);
                            homeModel.setColors(colors);
                            homeModel.setVariations(variations);
                            homeModel.setTodays_deal(todays_deal);
                            homeModel.setPublished(published);
                            homeModel.setFeatured(featured);
                            homeModel.setCurrent_stock(current_stock);
                            homeModel.setUnit(unit);
                            homeModel.setDiscount(discount);
                            homeModel.setDiscount_type(discount_type);
                            homeModel.setTax(tax);
                            homeModel.setTax_type(tax_type);
                            homeModel.setShipping_type(shipping_type);
                            homeModel.setShipping_cost(shipping_cost);
                            homeModel.setWeight(weight);
                            homeModel.setParcel_size(parcel_size);
                            homeModel.setNum_of_sale(num_of_sale);
                            homeModel.setMeta_title(meta_title);
                            homeModel.setMeta_description(meta_description);
                            homeModel.setMeta_img(meta_img);
                            homeModel.setPdf(pdf);
                            homeModel.setSlug(slug);
                            homeModel.setRating(rating);
                            homeModel.setCreated_at(created_at);
                            homeModel.setUpdated_at(updated_at);

                            homeArrayList.add(homeModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        homecategoryRecyclerView.setLayoutManager(layoutManager);

                        homeProductsGridAdapter = new HomeProductsGridAdapter(context, homeArrayList);
                        homeProductsGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHome = homeArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHome.getName());
                                intent.putExtra("ITEMPRICE", selectedHome.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHome.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHome.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHome.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        homecategoryRecyclerView.setAdapter(homeProductsGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHOmeKitchenAppliancesGrid(){
        homecategoryRecyclerView = findViewById(R.id.homecategoryRecyclerView);
        homeArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllLargeKitchenAppliances", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Home homeModel = new Home();
                            homeModel.setId(id);
                            homeModel.setName(name);
                            homeModel.setAdded_by(added_by);
                            homeModel.setCategory_id(category_id);
                            homeModel.setSubcategory_id(subcategory_id);
                            homeModel.setSubsubcategory_id(subsubcategory_id);
                            homeModel.setBrand_id(brand_id);
                            homeModel.setPhotos(photos);
                            homeModel.setThumbnail_img(thumbnail_img);
                            homeModel.setFeatured_img(featured_img);
                            homeModel.setFlash_deal_img(flash_deal_img);
                            homeModel.setVideo_provider(video_provider);
                            homeModel.setVideo_link(video_link);
                            homeModel.setTags(tags);
                            homeModel.setDescription(description);
                            homeModel.setUnit_price(unit_price);
                            homeModel.setPurchase_price(purchase_price);
                            homeModel.setChoice_options(choice_options);
                            homeModel.setColors(colors);
                            homeModel.setVariations(variations);
                            homeModel.setTodays_deal(todays_deal);
                            homeModel.setPublished(published);
                            homeModel.setFeatured(featured);
                            homeModel.setCurrent_stock(current_stock);
                            homeModel.setUnit(unit);
                            homeModel.setDiscount(discount);
                            homeModel.setDiscount_type(discount_type);
                            homeModel.setTax(tax);
                            homeModel.setTax_type(tax_type);
                            homeModel.setShipping_type(shipping_type);
                            homeModel.setShipping_cost(shipping_cost);
                            homeModel.setWeight(weight);
                            homeModel.setParcel_size(parcel_size);
                            homeModel.setNum_of_sale(num_of_sale);
                            homeModel.setMeta_title(meta_title);
                            homeModel.setMeta_description(meta_description);
                            homeModel.setMeta_img(meta_img);
                            homeModel.setPdf(pdf);
                            homeModel.setSlug(slug);
                            homeModel.setRating(rating);
                            homeModel.setCreated_at(created_at);
                            homeModel.setUpdated_at(updated_at);

                            homeArrayList.add(homeModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        homecategoryRecyclerView.setLayoutManager(layoutManager);

                        homeProductsGridAdapter = new HomeProductsGridAdapter(context, homeArrayList);
                        homeProductsGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHome = homeArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHome.getName());
                                intent.putExtra("ITEMPRICE", selectedHome.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHome.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHome.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHome.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        homecategoryRecyclerView.setAdapter(homeProductsGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHomeOthersGrid(){
        homecategoryRecyclerView = findViewById(R.id.homecategoryRecyclerView);
        homeArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHomeAppliancesOthers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Home homeModel = new Home();
                            homeModel.setId(id);
                            homeModel.setName(name);
                            homeModel.setAdded_by(added_by);
                            homeModel.setCategory_id(category_id);
                            homeModel.setSubcategory_id(subcategory_id);
                            homeModel.setSubsubcategory_id(subsubcategory_id);
                            homeModel.setBrand_id(brand_id);
                            homeModel.setPhotos(photos);
                            homeModel.setThumbnail_img(thumbnail_img);
                            homeModel.setFeatured_img(featured_img);
                            homeModel.setFlash_deal_img(flash_deal_img);
                            homeModel.setVideo_provider(video_provider);
                            homeModel.setVideo_link(video_link);
                            homeModel.setTags(tags);
                            homeModel.setDescription(description);
                            homeModel.setUnit_price(unit_price);
                            homeModel.setPurchase_price(purchase_price);
                            homeModel.setChoice_options(choice_options);
                            homeModel.setColors(colors);
                            homeModel.setVariations(variations);
                            homeModel.setTodays_deal(todays_deal);
                            homeModel.setPublished(published);
                            homeModel.setFeatured(featured);
                            homeModel.setCurrent_stock(current_stock);
                            homeModel.setUnit(unit);
                            homeModel.setDiscount(discount);
                            homeModel.setDiscount_type(discount_type);
                            homeModel.setTax(tax);
                            homeModel.setTax_type(tax_type);
                            homeModel.setShipping_type(shipping_type);
                            homeModel.setShipping_cost(shipping_cost);
                            homeModel.setWeight(weight);
                            homeModel.setParcel_size(parcel_size);
                            homeModel.setNum_of_sale(num_of_sale);
                            homeModel.setMeta_title(meta_title);
                            homeModel.setMeta_description(meta_description);
                            homeModel.setMeta_img(meta_img);
                            homeModel.setPdf(pdf);
                            homeModel.setSlug(slug);
                            homeModel.setRating(rating);
                            homeModel.setCreated_at(created_at);
                            homeModel.setUpdated_at(updated_at);

                            homeArrayList.add(homeModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        homecategoryRecyclerView.setLayoutManager(layoutManager);

                        homeProductsGridAdapter = new HomeProductsGridAdapter(context, homeArrayList);
                        homeProductsGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHome = homeArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHome.getName());
                                intent.putExtra("ITEMPRICE", selectedHome.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHome.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHome.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHome.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        homecategoryRecyclerView.setAdapter(homeProductsGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHomeTvGrid(){
        homecategoryRecyclerView = findViewById(R.id.homecategoryRecyclerView);
        homeArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHomeAppliancesTV", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Home homeModel = new Home();
                            homeModel.setId(id);
                            homeModel.setName(name);
                            homeModel.setAdded_by(added_by);
                            homeModel.setCategory_id(category_id);
                            homeModel.setSubcategory_id(subcategory_id);
                            homeModel.setSubsubcategory_id(subsubcategory_id);
                            homeModel.setBrand_id(brand_id);
                            homeModel.setPhotos(photos);
                            homeModel.setThumbnail_img(thumbnail_img);
                            homeModel.setFeatured_img(featured_img);
                            homeModel.setFlash_deal_img(flash_deal_img);
                            homeModel.setVideo_provider(video_provider);
                            homeModel.setVideo_link(video_link);
                            homeModel.setTags(tags);
                            homeModel.setDescription(description);
                            homeModel.setUnit_price(unit_price);
                            homeModel.setPurchase_price(purchase_price);
                            homeModel.setChoice_options(choice_options);
                            homeModel.setColors(colors);
                            homeModel.setVariations(variations);
                            homeModel.setTodays_deal(todays_deal);
                            homeModel.setPublished(published);
                            homeModel.setFeatured(featured);
                            homeModel.setCurrent_stock(current_stock);
                            homeModel.setUnit(unit);
                            homeModel.setDiscount(discount);
                            homeModel.setDiscount_type(discount_type);
                            homeModel.setTax(tax);
                            homeModel.setTax_type(tax_type);
                            homeModel.setShipping_type(shipping_type);
                            homeModel.setShipping_cost(shipping_cost);
                            homeModel.setWeight(weight);
                            homeModel.setParcel_size(parcel_size);
                            homeModel.setNum_of_sale(num_of_sale);
                            homeModel.setMeta_title(meta_title);
                            homeModel.setMeta_description(meta_description);
                            homeModel.setMeta_img(meta_img);
                            homeModel.setPdf(pdf);
                            homeModel.setSlug(slug);
                            homeModel.setRating(rating);
                            homeModel.setCreated_at(created_at);
                            homeModel.setUpdated_at(updated_at);

                            homeArrayList.add(homeModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        homecategoryRecyclerView.setLayoutManager(layoutManager);

                        homeProductsGridAdapter = new HomeProductsGridAdapter(context, homeArrayList);
                        homeProductsGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHome = homeArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHome.getName());
                                intent.putExtra("ITEMPRICE", selectedHome.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHome.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHome.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHome.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        homecategoryRecyclerView.setAdapter(homeProductsGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
}
