package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.ChatViewPagerAdapter;
import com.example.jigijog_mobile_app.fragments.ChatFragment;
import com.example.jigijog_mobile_app.fragments.NotificationFragment;
import com.example.jigijog_mobile_app.utils.Tools;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

public class ChatActivity extends AppCompatActivity {
    private View view;

    private Context context;
    private ImageView iv_mychatBack;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ChatViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Tools.setSystemBarColor(this, R.color.deep_orange_400);
        Tools.setSystemBarLight(this);
        initializeUI();
    }

    private void initializeUI() {
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.view_pager);
        iv_mychatBack = findViewById(R.id.iv_mychatBack);

        viewPagerAdapter = new ChatViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new ChatFragment(), "Chat");
        viewPagerAdapter.addFragment(new NotificationFragment(), "Notification");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();

        iv_mychatBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Chat");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_chat_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(0)).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Notification");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_notifications_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setCustomView(tabTwo);

    }
}
