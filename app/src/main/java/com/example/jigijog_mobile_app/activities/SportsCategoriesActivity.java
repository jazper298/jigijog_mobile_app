package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.ComputerProductAdapter;
import com.example.jigijog_mobile_app.adapters.KidsProductAdapter;
import com.example.jigijog_mobile_app.adapters.SportsProductAdapter;
import com.example.jigijog_mobile_app.adapters.SportsProductGridAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Computer;
import com.example.jigijog_mobile_app.models.Kids;
import com.example.jigijog_mobile_app.models.Sports;
import com.example.jigijog_mobile_app.models.Women;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class SportsCategoriesActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private String message;

    private ImageView iv_sportscategoryBack, iv_sportscategoryGrid,iv_sportscategoryMore,iv_sportscategoryCart;
    private TextView tv_sportscategoryName;

    private ConstraintLayout emptyIndicator;


    private RecyclerView sportscategoryRecyclerView;
    private ArrayList<Sports> sportsArrayList;
    private SportsProductAdapter sportsProductAdapter;
    private SportsProductGridAdapter sportsProductGridAdapter;

    private Sports selectedSports = new Sports();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();
        if(message.equals("Athletic & Outdoor Accessories")) {
            tv_sportscategoryName.setText( message);
            loadSportsAthletic();
            iv_sportscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadSportsAthleticGrid();
                }
            });
        }else if (message.equals("Camping & Hiking")){
            tv_sportscategoryName.setText( message);
            loadSportsCamping();
            iv_sportscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadSportsCampingGrid();
                }
            });
        }else if (message.equals("Cycling")){
            tv_sportscategoryName.setText( message);
            loadSportsCycling();
            iv_sportscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadSportsCyclingGrid();
                }
            });
        }else if (message.equals("Fitness Supplies")){
            tv_sportscategoryName.setText( message);
            loadSportsFitness();
            iv_sportscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadSportsFitnessGrid();
                }
            });
        }
        else if (message.equals("Sneakers")){
            tv_sportscategoryName.setText( message);
            loadSportsSneakers();
            iv_sportscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadSportsSneakersGrid();
                }
            });
        }
    }

    private void initializeUI() {
        emptyIndicator = findViewById(R.id.view_Empty);
        tv_sportscategoryName = findViewById(R.id.tv_sportscategoryName);
        iv_sportscategoryGrid = findViewById(R.id.iv_sportscategoryGrid);
        iv_sportscategoryBack = findViewById(R.id.iv_sportscategoryBack);
        iv_sportscategoryMore = findViewById(R.id.iv_sportscategoryMore);
        iv_sportscategoryCart = findViewById(R.id.iv_sportscategoryCart);
        iv_sportscategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_sportscategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_sportscategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void loadSportsAthletic(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllAthleticAndOutdoorAccs", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductAdapter = new SportsProductAdapter(context, sportsArrayList);
                        sportsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsCamping(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCampingAndHiking", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductAdapter = new SportsProductAdapter(context, sportsArrayList);
                        sportsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsCycling(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCycling", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductAdapter = new SportsProductAdapter(context, sportsArrayList);
                        sportsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsFitness(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllFitnessSupplies", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductAdapter = new SportsProductAdapter(context, sportsArrayList);
                        sportsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsSneakers(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllSneakers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductAdapter = new SportsProductAdapter(context, sportsArrayList);
                        sportsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsAthleticGrid(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllAthleticAndOutdoorAccs", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductGridAdapter = new SportsProductGridAdapter(context, sportsArrayList);
                        sportsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsCampingGrid(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCampingAndHiking", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductGridAdapter = new SportsProductGridAdapter(context, sportsArrayList);
                        sportsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsCyclingGrid(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCycling", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductGridAdapter = new SportsProductGridAdapter(context, sportsArrayList);
                        sportsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsFitnessGrid(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllFitnessSupplies", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductGridAdapter = new SportsProductGridAdapter(context, sportsArrayList);
                        sportsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsSneakersGrid(){
        sportscategoryRecyclerView = findViewById(R.id.sportscategoryRecyclerView);
        sportsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllSneakers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        sportscategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductGridAdapter = new SportsProductGridAdapter(context, sportsArrayList);
                        sportsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtra("ITEMPRICE", selectedSports.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedSports.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedSports.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedSports.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        sportscategoryRecyclerView.setAdapter(sportsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
}
