package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.AssordtedGridAdapter;
import com.example.jigijog_mobile_app.adapters.AssortedAdapter;
import com.example.jigijog_mobile_app.adapters.FeaturedAdapter;
import com.example.jigijog_mobile_app.data.DataGenerator;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Assorted;
import com.example.jigijog_mobile_app.models.Featured;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class AssortedProductsActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private ImageView iv_assortedBack,iv_assortedMore, iv_assortedCart;
    private ImageView iv_assortedGrid;

    private RecyclerView recyclerView;
    private ConstraintLayout emptyIndicator;

    private int item_per_display = 10;


    private RecyclerView assortedRecyclerview;
    private ArrayList<Assorted> assortedArrayList;
    private AssortedAdapter assortedAdapter;
    private AssordtedGridAdapter assordtedGridAdapter;

    private Assorted selectedAssorted = new Assorted();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assorted_products);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = getApplicationContext();

    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
        loadAssortedProducts();
    }

    private void initializeUI() {
        iv_assortedBack = findViewById(R.id.iv_assortedBack);
        emptyIndicator = findViewById(R.id.view_Empty);
        iv_assortedGrid = findViewById(R.id.iv_assortedGrid);
        iv_assortedMore = findViewById(R.id.iv_assortedMore);
        iv_assortedCart = findViewById(R.id.iv_assortedCart);
        iv_assortedCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_assortedGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAssortedGridProducts();
            }
        });
        iv_assortedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        iv_assortedMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    private void loadAssortedProducts() {
        assortedRecyclerview = findViewById(R.id.assortedRecyclerView);
        assortedArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAssortedItems", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Assorted assortedModel = new Assorted();
                        assortedModel.setId(id);
                        assortedModel.setName(name);
                        assortedModel.setAdded_by(added_by);
                        assortedModel.setCategory_id(category_id);
                        assortedModel.setSubcategory_id(subcategory_id);
                        assortedModel.setSubsubcategory_id(subsubcategory_id);
                        assortedModel.setBrand_id(brand_id);
                        assortedModel.setPhotos(photos);
                        assortedModel.setThumbnail_img(thumbnail_img);
                        assortedModel.setFeatured_img(featured_img);
                        assortedModel.setFlash_deal_img(flash_deal_img);
                        assortedModel.setVideo_provider(video_provider);
                        assortedModel.setVideo_link(video_link);
                        assortedModel.setTags(tags);
                        assortedModel.setDescription(description);
                        assortedModel.setUnit_price(unit_price);
                        assortedModel.setPurchase_price(purchase_price);
                        assortedModel.setChoice_options(choice_options);
                        assortedModel.setColors(colors);
                        assortedModel.setVariations(variations);
                        assortedModel.setTodays_deal(todays_deal);
                        assortedModel.setPublished(published);
                        assortedModel.setFeatured(featured);
                        assortedModel.setCurrent_stock(current_stock);
                        assortedModel.setUnit(unit);
                        assortedModel.setDiscount(discount);
                        assortedModel.setDiscount_type(discount_type);
                        assortedModel.setTax(tax);
                        assortedModel.setTax_type(tax_type);
                        assortedModel.setShipping_type(shipping_type);
                        assortedModel.setShipping_cost(shipping_cost);
                        assortedModel.setWeight(weight);
                        assortedModel.setParcel_size(parcel_size);
                        assortedModel.setNum_of_sale(num_of_sale);
                        assortedModel.setMeta_title(meta_title);
                        assortedModel.setMeta_description(meta_description);
                        assortedModel.setMeta_img(meta_img);
                        assortedModel.setPdf(pdf);
                        assortedModel.setSlug(slug);
                        assortedModel.setRating(rating);
                        assortedModel.setCreated_at(created_at);
                        assortedModel.setUpdated_at(updated_at);

                        assortedArrayList.add(assortedModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,  2, GridLayoutManager.VERTICAL, false);
                    assortedRecyclerview.setLayoutManager(layoutManager);

                    assortedAdapter = new AssortedAdapter(context, assortedArrayList);
                    assortedAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedAssorted = assortedArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedAssorted.getName());
                            intent.putExtra("ITEMPRICE", selectedAssorted.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedAssorted.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedAssorted.getFeatured_img());
                            intent.putExtra("ITEMIDES",selectedAssorted.getDescription());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    assortedRecyclerview.setAdapter(assortedAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });



    }


    private void loadAssortedGridProducts() {
        assortedRecyclerview = findViewById(R.id.assortedRecyclerView);
        assortedArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAssortedItems", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Assorted assortedModel = new Assorted();
                        assortedModel.setId(id);
                        assortedModel.setName(name);
                        assortedModel.setAdded_by(added_by);
                        assortedModel.setCategory_id(category_id);
                        assortedModel.setSubcategory_id(subcategory_id);
                        assortedModel.setSubsubcategory_id(subsubcategory_id);
                        assortedModel.setBrand_id(brand_id);
                        assortedModel.setPhotos(photos);
                        assortedModel.setThumbnail_img(thumbnail_img);
                        assortedModel.setFeatured_img(featured_img);
                        assortedModel.setFlash_deal_img(flash_deal_img);
                        assortedModel.setVideo_provider(video_provider);
                        assortedModel.setVideo_link(video_link);
                        assortedModel.setTags(tags);
                        assortedModel.setDescription(description);
                        assortedModel.setUnit_price(unit_price);
                        assortedModel.setPurchase_price(purchase_price);
                        assortedModel.setChoice_options(choice_options);
                        assortedModel.setColors(colors);
                        assortedModel.setVariations(variations);
                        assortedModel.setTodays_deal(todays_deal);
                        assortedModel.setPublished(published);
                        assortedModel.setFeatured(featured);
                        assortedModel.setCurrent_stock(current_stock);
                        assortedModel.setUnit(unit);
                        assortedModel.setDiscount(discount);
                        assortedModel.setDiscount_type(discount_type);
                        assortedModel.setTax(tax);
                        assortedModel.setTax_type(tax_type);
                        assortedModel.setShipping_type(shipping_type);
                        assortedModel.setShipping_cost(shipping_cost);
                        assortedModel.setWeight(weight);
                        assortedModel.setParcel_size(parcel_size);
                        assortedModel.setNum_of_sale(num_of_sale);
                        assortedModel.setMeta_title(meta_title);
                        assortedModel.setMeta_description(meta_description);
                        assortedModel.setMeta_img(meta_img);
                        assortedModel.setPdf(pdf);
                        assortedModel.setSlug(slug);
                        assortedModel.setRating(rating);
                        assortedModel.setCreated_at(created_at);
                        assortedModel.setUpdated_at(updated_at);

                        assortedArrayList.add(assortedModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                    assortedRecyclerview.setLayoutManager(layoutManager);

                    assordtedGridAdapter = new AssordtedGridAdapter(context, assortedArrayList);
                    assordtedGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedAssorted = assortedArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedAssorted.getName());
                            intent.putExtra("ITEMPRICE", selectedAssorted.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedAssorted.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedAssorted.getFeatured_img());
                            intent.putExtra("ITEMIDES",selectedAssorted.getDescription());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    assortedRecyclerview.setAdapter(assordtedGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });


    }
}
