package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.AssordtedGridAdapter;
import com.example.jigijog_mobile_app.adapters.ComputerProductAdapter;
import com.example.jigijog_mobile_app.adapters.ComputerProductGridAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Assorted;
import com.example.jigijog_mobile_app.models.Computer;
import com.example.jigijog_mobile_app.models.Women;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ComputerCategoriesActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private String message;

    private ImageView iv_computercategoryBack, iv_computercategoryGrid,iv_computercategoryMore,iv_computercategoryCart;
    private TextView tv_computercategoryName;

    private ConstraintLayout emptyIndicator;


    private RecyclerView computercategoryRecyclerView;
    private ArrayList<Computer> computerArrayList;
    private ComputerProductAdapter computerProductAdapter;
    private ComputerProductGridAdapter computerProductGridAdapter;

    private Computer selectedComputer = new Computer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computer_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();

        if(message.equals("Acer Products")) {
            tv_computercategoryName.setText( message);
            loadComputerAcerProducts();
            iv_computercategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadComputerAcerProductsGrid();
                }
            });
        }else if (message.equals("Asus Products")){
            tv_computercategoryName.setText( message);
            loadComputerAsusProducts();
            iv_computercategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadComputerAsusProductsGrid();
                }
            });
        }else if (message.equals("Hewlett-Packard")){
            tv_computercategoryName.setText( message);
            loadComputerHewlettPackard();
            iv_computercategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadComputerHewlettPackardGrid();
                }
            });
        }else if (message.equals("Dell Hardware")){
            tv_computercategoryName.setText( message);
            loadComputerDellHardware();
            iv_computercategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadComputerDellHardwareGrid();
                }
            });
        }
        else if (message.equals("Intel Products")){
            tv_computercategoryName.setText( message);
            loadComputerIntelProducts();
            iv_computercategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadComputerIntelProductsGrid();
                }
            });
        }
        else if (message.equals("Accessories")){
            tv_computercategoryName.setText( message);
            loadComputerAccessories();
            iv_computercategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadComputerAccessoriesGrid();
                }
            });
        }
    }

    private void initializeUI() {
        emptyIndicator = findViewById(R.id.view_Empty);
        tv_computercategoryName = findViewById(R.id.tv_computercategoryName);
        iv_computercategoryGrid = findViewById(R.id.iv_computercategoryGrid);
        iv_computercategoryCart = findViewById(R.id.iv_computercategoryCart);
        iv_computercategoryBack = findViewById(R.id.iv_computercategoryBack);
        iv_computercategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_computercategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_computercategoryMore = findViewById(R.id.iv_computercategoryMore);
        iv_computercategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void loadComputerAcerProducts() {
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerAcer", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductAdapter = new ComputerProductAdapter(context, computerArrayList);
                        computerProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerAsusProducts() {
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerAsus", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductAdapter = new ComputerProductAdapter(context, computerArrayList);
                        computerProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerHewlettPackard() {
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerHP", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductAdapter = new ComputerProductAdapter(context, computerArrayList);
                        computerProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerDellHardware() {
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerDell", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductAdapter = new ComputerProductAdapter(context, computerArrayList);
                        computerProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerIntelProducts() {
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerIntel", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductAdapter = new ComputerProductAdapter(context, computerArrayList);
                        computerProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerAccessories(){
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerAccessories", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductAdapter = new ComputerProductAdapter(context, computerArrayList);
                        computerProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerAcerProductsGrid(){
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerAcer", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals(""))
                    emptyIndicator.setVisibility(View.VISIBLE);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Computer computerModel = new Computer();
                        computerModel.setId(id);
                        computerModel.setName(name);
                        computerModel.setAdded_by(added_by);
                        computerModel.setCategory_id(category_id);
                        computerModel.setSubcategory_id(subcategory_id);
                        computerModel.setSubsubcategory_id(subsubcategory_id);
                        computerModel.setBrand_id(brand_id);
                        computerModel.setPhotos(photos);
                        computerModel.setThumbnail_img(thumbnail_img);
                        computerModel.setFeatured_img(featured_img);
                        computerModel.setFlash_deal_img(flash_deal_img);
                        computerModel.setVideo_provider(video_provider);
                        computerModel.setVideo_link(video_link);
                        computerModel.setTags(tags);
                        computerModel.setDescription(description);
                        computerModel.setUnit_price(unit_price);
                        computerModel.setPurchase_price(purchase_price);
                        computerModel.setChoice_options(choice_options);
                        computerModel.setColors(colors);
                        computerModel.setVariations(variations);
                        computerModel.setTodays_deal(todays_deal);
                        computerModel.setPublished(published);
                        computerModel.setFeatured(featured);
                        computerModel.setCurrent_stock(current_stock);
                        computerModel.setUnit(unit);
                        computerModel.setDiscount(discount);
                        computerModel.setDiscount_type(discount_type);
                        computerModel.setTax(tax);
                        computerModel.setTax_type(tax_type);
                        computerModel.setShipping_type(shipping_type);
                        computerModel.setShipping_cost(shipping_cost);
                        computerModel.setWeight(weight);
                        computerModel.setParcel_size(parcel_size);
                        computerModel.setNum_of_sale(num_of_sale);
                        computerModel.setMeta_title(meta_title);
                        computerModel.setMeta_description(meta_description);
                        computerModel.setMeta_img(meta_img);
                        computerModel.setPdf(pdf);
                        computerModel.setSlug(slug);
                        computerModel.setRating(rating);
                        computerModel.setCreated_at(created_at);
                        computerModel.setUpdated_at(updated_at);

                        computerArrayList.add(computerModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                    computercategoryRecyclerView.setLayoutManager(layoutManager);

                    computerProductGridAdapter = new ComputerProductGridAdapter(context, computerArrayList);
                    computerProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedComputer = computerArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedComputer.getName());
                            intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedComputer.getFeatured_img());
                            intent.putExtra("ITEMIDES",selectedComputer.getDescription());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    computercategoryRecyclerView.setAdapter(computerProductGridAdapter);
                    emptyIndicator.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerAsusProductsGrid(){
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerAsus", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")) {
                    emptyIndicator.setVisibility(View.VISIBLE);
                } else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductGridAdapter = new ComputerProductGridAdapter(context, computerArrayList);
                        computerProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE", selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES", selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerHewlettPackardGrid(){
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerHP", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")) {
                    emptyIndicator.setVisibility(View.VISIBLE);
                } else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductGridAdapter = new ComputerProductGridAdapter(context, computerArrayList);
                        computerProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE", selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES", selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerDellHardwareGrid(){
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerDell", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")) {
                    emptyIndicator.setVisibility(View.VISIBLE);
                } else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductGridAdapter = new ComputerProductGridAdapter(context, computerArrayList);
                        computerProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE", selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES", selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerIntelProductsGrid(){
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerIntel", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")) {
                    emptyIndicator.setVisibility(View.VISIBLE);
                } else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductGridAdapter = new ComputerProductGridAdapter(context, computerArrayList);
                        computerProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE", selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES", selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerAccessoriesGrid(){
        computercategoryRecyclerView = findViewById(R.id.computercategoryRecyclerView);
        computerArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllComputerAccessories", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")) {
                    emptyIndicator.setVisibility(View.VISIBLE);
                } else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Computer computerModel = new Computer();
                            computerModel.setId(id);
                            computerModel.setName(name);
                            computerModel.setAdded_by(added_by);
                            computerModel.setCategory_id(category_id);
                            computerModel.setSubcategory_id(subcategory_id);
                            computerModel.setSubsubcategory_id(subsubcategory_id);
                            computerModel.setBrand_id(brand_id);
                            computerModel.setPhotos(photos);
                            computerModel.setThumbnail_img(thumbnail_img);
                            computerModel.setFeatured_img(featured_img);
                            computerModel.setFlash_deal_img(flash_deal_img);
                            computerModel.setVideo_provider(video_provider);
                            computerModel.setVideo_link(video_link);
                            computerModel.setTags(tags);
                            computerModel.setDescription(description);
                            computerModel.setUnit_price(unit_price);
                            computerModel.setPurchase_price(purchase_price);
                            computerModel.setChoice_options(choice_options);
                            computerModel.setColors(colors);
                            computerModel.setVariations(variations);
                            computerModel.setTodays_deal(todays_deal);
                            computerModel.setPublished(published);
                            computerModel.setFeatured(featured);
                            computerModel.setCurrent_stock(current_stock);
                            computerModel.setUnit(unit);
                            computerModel.setDiscount(discount);
                            computerModel.setDiscount_type(discount_type);
                            computerModel.setTax(tax);
                            computerModel.setTax_type(tax_type);
                            computerModel.setShipping_type(shipping_type);
                            computerModel.setShipping_cost(shipping_cost);
                            computerModel.setWeight(weight);
                            computerModel.setParcel_size(parcel_size);
                            computerModel.setNum_of_sale(num_of_sale);
                            computerModel.setMeta_title(meta_title);
                            computerModel.setMeta_description(meta_description);
                            computerModel.setMeta_img(meta_img);
                            computerModel.setPdf(pdf);
                            computerModel.setSlug(slug);
                            computerModel.setRating(rating);
                            computerModel.setCreated_at(created_at);
                            computerModel.setUpdated_at(updated_at);

                            computerArrayList.add(computerModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        computercategoryRecyclerView.setLayoutManager(layoutManager);

                        computerProductGridAdapter = new ComputerProductGridAdapter(context, computerArrayList);
                        computerProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedComputer = computerArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedComputer.getName());
                                intent.putExtra("ITEMPRICE", selectedComputer.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedComputer.getDiscount());
                                intent.putExtra("ITEMIMAGE", selectedComputer.getFeatured_img());
                                intent.putExtra("ITEMIDES", selectedComputer.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        computercategoryRecyclerView.setAdapter(computerProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

}
