package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.MobileProductAdapter;
import com.example.jigijog_mobile_app.adapters.MobileProductGridAdapter;
import com.example.jigijog_mobile_app.adapters.SportsProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Mobile;
import com.example.jigijog_mobile_app.models.Sports;
import com.example.jigijog_mobile_app.models.Women;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MobileCategoriesActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private String message;

    private ImageView iv_mobilecategoryBack, iv_mobilecategoryGrid,iv_mobilecategoryMore, iv_mobilecategoryCart;
    private TextView tv_mobilecategoryName;

    private ConstraintLayout emptyIndicator;


    private RecyclerView mobilecategoryRecyclerView;
    private ArrayList<Mobile> mobileArrayList;
    private MobileProductAdapter mobileProductAdapter;
    private MobileProductGridAdapter mobileProductGridAdapter;

    private Mobile selectedMobile = new Mobile();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();
        if(message.equals("Chargers")) {
            tv_mobilecategoryName.setText( message);
            loadMobileChargers();
            iv_mobilecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMobileChargersGrid();
                }
            });
        }else if (message.equals("Case")){
            tv_mobilecategoryName.setText( message);
            loadMobileCases();
            iv_mobilecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMobileCasesGrid();
                }
            });
        }else if (message.equals("Cellphones/Phones")) {
            tv_mobilecategoryName.setText(message);
            loadMobilePhones();
            iv_mobilecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMobilePhonesGrid();
                }
            });
        }
    }

    private void initializeUI() {
        emptyIndicator = findViewById(R.id.view_Empty);
        tv_mobilecategoryName = findViewById(R.id.tv_mobilecategoryName);
        iv_mobilecategoryGrid = findViewById(R.id.iv_mobilecategoryGrid);
        iv_mobilecategoryBack = findViewById(R.id.iv_mobilecategoryBack);
        iv_mobilecategoryCart = findViewById(R.id.iv_mobilecategoryCart);
        iv_mobilecategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_mobilecategoryMore = findViewById(R.id.iv_mobilecategoryMore);
        iv_mobilecategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_mobilecategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void loadMobileChargers(){
        mobilecategoryRecyclerView = findViewById(R.id.mobilecategoryRecyclerView);
        mobileArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllChargers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Mobile mobileModel = new Mobile();
                            mobileModel.setId(id);
                            mobileModel.setName(name);
                            mobileModel.setAdded_by(added_by);
                            mobileModel.setCategory_id(category_id);
                            mobileModel.setSubcategory_id(subcategory_id);
                            mobileModel.setSubsubcategory_id(subsubcategory_id);
                            mobileModel.setBrand_id(brand_id);
                            mobileModel.setPhotos(photos);
                            mobileModel.setThumbnail_img(thumbnail_img);
                            mobileModel.setFeatured_img(featured_img);
                            mobileModel.setFlash_deal_img(flash_deal_img);
                            mobileModel.setVideo_provider(video_provider);
                            mobileModel.setVideo_link(video_link);
                            mobileModel.setTags(tags);
                            mobileModel.setDescription(description);
                            mobileModel.setUnit_price(unit_price);
                            mobileModel.setPurchase_price(purchase_price);
                            mobileModel.setChoice_options(choice_options);
                            mobileModel.setColors(colors);
                            mobileModel.setVariations(variations);
                            mobileModel.setTodays_deal(todays_deal);
                            mobileModel.setPublished(published);
                            mobileModel.setFeatured(featured);
                            mobileModel.setCurrent_stock(current_stock);
                            mobileModel.setUnit(unit);
                            mobileModel.setDiscount(discount);
                            mobileModel.setDiscount_type(discount_type);
                            mobileModel.setTax(tax);
                            mobileModel.setTax_type(tax_type);
                            mobileModel.setShipping_type(shipping_type);
                            mobileModel.setShipping_cost(shipping_cost);
                            mobileModel.setWeight(weight);
                            mobileModel.setParcel_size(parcel_size);
                            mobileModel.setNum_of_sale(num_of_sale);
                            mobileModel.setMeta_title(meta_title);
                            mobileModel.setMeta_description(meta_description);
                            mobileModel.setMeta_img(meta_img);
                            mobileModel.setPdf(pdf);
                            mobileModel.setSlug(slug);
                            mobileModel.setRating(rating);
                            mobileModel.setCreated_at(created_at);
                            mobileModel.setUpdated_at(updated_at);

                            mobileArrayList.add(mobileModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        mobilecategoryRecyclerView.setLayoutManager(layoutManager);

                        mobileProductAdapter = new MobileProductAdapter(context, mobileArrayList);
                        mobileProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedMobile = mobileArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedMobile.getName());
                                intent.putExtra("ITEMPRICE", selectedMobile.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedMobile.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedMobile.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedMobile.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        mobilecategoryRecyclerView.setAdapter(mobileProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadMobileCases(){
        mobilecategoryRecyclerView = findViewById(R.id.mobilecategoryRecyclerView);
        mobileArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCase", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Mobile mobileModel = new Mobile();
                            mobileModel.setId(id);
                            mobileModel.setName(name);
                            mobileModel.setAdded_by(added_by);
                            mobileModel.setCategory_id(category_id);
                            mobileModel.setSubcategory_id(subcategory_id);
                            mobileModel.setSubsubcategory_id(subsubcategory_id);
                            mobileModel.setBrand_id(brand_id);
                            mobileModel.setPhotos(photos);
                            mobileModel.setThumbnail_img(thumbnail_img);
                            mobileModel.setFeatured_img(featured_img);
                            mobileModel.setFlash_deal_img(flash_deal_img);
                            mobileModel.setVideo_provider(video_provider);
                            mobileModel.setVideo_link(video_link);
                            mobileModel.setTags(tags);
                            mobileModel.setDescription(description);
                            mobileModel.setUnit_price(unit_price);
                            mobileModel.setPurchase_price(purchase_price);
                            mobileModel.setChoice_options(choice_options);
                            mobileModel.setColors(colors);
                            mobileModel.setVariations(variations);
                            mobileModel.setTodays_deal(todays_deal);
                            mobileModel.setPublished(published);
                            mobileModel.setFeatured(featured);
                            mobileModel.setCurrent_stock(current_stock);
                            mobileModel.setUnit(unit);
                            mobileModel.setDiscount(discount);
                            mobileModel.setDiscount_type(discount_type);
                            mobileModel.setTax(tax);
                            mobileModel.setTax_type(tax_type);
                            mobileModel.setShipping_type(shipping_type);
                            mobileModel.setShipping_cost(shipping_cost);
                            mobileModel.setWeight(weight);
                            mobileModel.setParcel_size(parcel_size);
                            mobileModel.setNum_of_sale(num_of_sale);
                            mobileModel.setMeta_title(meta_title);
                            mobileModel.setMeta_description(meta_description);
                            mobileModel.setMeta_img(meta_img);
                            mobileModel.setPdf(pdf);
                            mobileModel.setSlug(slug);
                            mobileModel.setRating(rating);
                            mobileModel.setCreated_at(created_at);
                            mobileModel.setUpdated_at(updated_at);

                            mobileArrayList.add(mobileModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        mobilecategoryRecyclerView.setLayoutManager(layoutManager);

                        mobileProductAdapter = new MobileProductAdapter(context, mobileArrayList);
                        mobileProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedMobile = mobileArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedMobile.getName());
                                intent.putExtra("ITEMPRICE", selectedMobile.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedMobile.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedMobile.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedMobile.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        mobilecategoryRecyclerView.setAdapter(mobileProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadMobilePhones(){
        mobilecategoryRecyclerView = findViewById(R.id.mobilecategoryRecyclerView);
        mobileArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCellphonesPhones", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Mobile mobileModel = new Mobile();
                            mobileModel.setId(id);
                            mobileModel.setName(name);
                            mobileModel.setAdded_by(added_by);
                            mobileModel.setCategory_id(category_id);
                            mobileModel.setSubcategory_id(subcategory_id);
                            mobileModel.setSubsubcategory_id(subsubcategory_id);
                            mobileModel.setBrand_id(brand_id);
                            mobileModel.setPhotos(photos);
                            mobileModel.setThumbnail_img(thumbnail_img);
                            mobileModel.setFeatured_img(featured_img);
                            mobileModel.setFlash_deal_img(flash_deal_img);
                            mobileModel.setVideo_provider(video_provider);
                            mobileModel.setVideo_link(video_link);
                            mobileModel.setTags(tags);
                            mobileModel.setDescription(description);
                            mobileModel.setUnit_price(unit_price);
                            mobileModel.setPurchase_price(purchase_price);
                            mobileModel.setChoice_options(choice_options);
                            mobileModel.setColors(colors);
                            mobileModel.setVariations(variations);
                            mobileModel.setTodays_deal(todays_deal);
                            mobileModel.setPublished(published);
                            mobileModel.setFeatured(featured);
                            mobileModel.setCurrent_stock(current_stock);
                            mobileModel.setUnit(unit);
                            mobileModel.setDiscount(discount);
                            mobileModel.setDiscount_type(discount_type);
                            mobileModel.setTax(tax);
                            mobileModel.setTax_type(tax_type);
                            mobileModel.setShipping_type(shipping_type);
                            mobileModel.setShipping_cost(shipping_cost);
                            mobileModel.setWeight(weight);
                            mobileModel.setParcel_size(parcel_size);
                            mobileModel.setNum_of_sale(num_of_sale);
                            mobileModel.setMeta_title(meta_title);
                            mobileModel.setMeta_description(meta_description);
                            mobileModel.setMeta_img(meta_img);
                            mobileModel.setPdf(pdf);
                            mobileModel.setSlug(slug);
                            mobileModel.setRating(rating);
                            mobileModel.setCreated_at(created_at);
                            mobileModel.setUpdated_at(updated_at);

                            mobileArrayList.add(mobileModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        mobilecategoryRecyclerView.setLayoutManager(layoutManager);

                        mobileProductAdapter = new MobileProductAdapter(context, mobileArrayList);
                        mobileProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedMobile = mobileArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedMobile.getName());
                                intent.putExtra("ITEMPRICE", selectedMobile.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedMobile.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedMobile.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedMobile.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        mobilecategoryRecyclerView.setAdapter(mobileProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadMobileChargersGrid(){
        mobilecategoryRecyclerView = findViewById(R.id.mobilecategoryRecyclerView);
        mobileArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllChargers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Mobile mobileModel = new Mobile();
                            mobileModel.setId(id);
                            mobileModel.setName(name);
                            mobileModel.setAdded_by(added_by);
                            mobileModel.setCategory_id(category_id);
                            mobileModel.setSubcategory_id(subcategory_id);
                            mobileModel.setSubsubcategory_id(subsubcategory_id);
                            mobileModel.setBrand_id(brand_id);
                            mobileModel.setPhotos(photos);
                            mobileModel.setThumbnail_img(thumbnail_img);
                            mobileModel.setFeatured_img(featured_img);
                            mobileModel.setFlash_deal_img(flash_deal_img);
                            mobileModel.setVideo_provider(video_provider);
                            mobileModel.setVideo_link(video_link);
                            mobileModel.setTags(tags);
                            mobileModel.setDescription(description);
                            mobileModel.setUnit_price(unit_price);
                            mobileModel.setPurchase_price(purchase_price);
                            mobileModel.setChoice_options(choice_options);
                            mobileModel.setColors(colors);
                            mobileModel.setVariations(variations);
                            mobileModel.setTodays_deal(todays_deal);
                            mobileModel.setPublished(published);
                            mobileModel.setFeatured(featured);
                            mobileModel.setCurrent_stock(current_stock);
                            mobileModel.setUnit(unit);
                            mobileModel.setDiscount(discount);
                            mobileModel.setDiscount_type(discount_type);
                            mobileModel.setTax(tax);
                            mobileModel.setTax_type(tax_type);
                            mobileModel.setShipping_type(shipping_type);
                            mobileModel.setShipping_cost(shipping_cost);
                            mobileModel.setWeight(weight);
                            mobileModel.setParcel_size(parcel_size);
                            mobileModel.setNum_of_sale(num_of_sale);
                            mobileModel.setMeta_title(meta_title);
                            mobileModel.setMeta_description(meta_description);
                            mobileModel.setMeta_img(meta_img);
                            mobileModel.setPdf(pdf);
                            mobileModel.setSlug(slug);
                            mobileModel.setRating(rating);
                            mobileModel.setCreated_at(created_at);
                            mobileModel.setUpdated_at(updated_at);

                            mobileArrayList.add(mobileModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        mobilecategoryRecyclerView.setLayoutManager(layoutManager);

                        mobileProductGridAdapter = new MobileProductGridAdapter(context, mobileArrayList);
                        mobileProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedMobile = mobileArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedMobile.getName());
                                intent.putExtra("ITEMPRICE", selectedMobile.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedMobile.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedMobile.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedMobile.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        mobilecategoryRecyclerView.setAdapter(mobileProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadMobileCasesGrid(){
        mobilecategoryRecyclerView = findViewById(R.id.mobilecategoryRecyclerView);
        mobileArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCase", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Mobile mobileModel = new Mobile();
                            mobileModel.setId(id);
                            mobileModel.setName(name);
                            mobileModel.setAdded_by(added_by);
                            mobileModel.setCategory_id(category_id);
                            mobileModel.setSubcategory_id(subcategory_id);
                            mobileModel.setSubsubcategory_id(subsubcategory_id);
                            mobileModel.setBrand_id(brand_id);
                            mobileModel.setPhotos(photos);
                            mobileModel.setThumbnail_img(thumbnail_img);
                            mobileModel.setFeatured_img(featured_img);
                            mobileModel.setFlash_deal_img(flash_deal_img);
                            mobileModel.setVideo_provider(video_provider);
                            mobileModel.setVideo_link(video_link);
                            mobileModel.setTags(tags);
                            mobileModel.setDescription(description);
                            mobileModel.setUnit_price(unit_price);
                            mobileModel.setPurchase_price(purchase_price);
                            mobileModel.setChoice_options(choice_options);
                            mobileModel.setColors(colors);
                            mobileModel.setVariations(variations);
                            mobileModel.setTodays_deal(todays_deal);
                            mobileModel.setPublished(published);
                            mobileModel.setFeatured(featured);
                            mobileModel.setCurrent_stock(current_stock);
                            mobileModel.setUnit(unit);
                            mobileModel.setDiscount(discount);
                            mobileModel.setDiscount_type(discount_type);
                            mobileModel.setTax(tax);
                            mobileModel.setTax_type(tax_type);
                            mobileModel.setShipping_type(shipping_type);
                            mobileModel.setShipping_cost(shipping_cost);
                            mobileModel.setWeight(weight);
                            mobileModel.setParcel_size(parcel_size);
                            mobileModel.setNum_of_sale(num_of_sale);
                            mobileModel.setMeta_title(meta_title);
                            mobileModel.setMeta_description(meta_description);
                            mobileModel.setMeta_img(meta_img);
                            mobileModel.setPdf(pdf);
                            mobileModel.setSlug(slug);
                            mobileModel.setRating(rating);
                            mobileModel.setCreated_at(created_at);
                            mobileModel.setUpdated_at(updated_at);

                            mobileArrayList.add(mobileModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        mobilecategoryRecyclerView.setLayoutManager(layoutManager);

                        mobileProductGridAdapter = new MobileProductGridAdapter(context, mobileArrayList);
                        mobileProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedMobile = mobileArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedMobile.getName());
                                intent.putExtra("ITEMPRICE", selectedMobile.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedMobile.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedMobile.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedMobile.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        mobilecategoryRecyclerView.setAdapter(mobileProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadMobilePhonesGrid(){
        mobilecategoryRecyclerView = findViewById(R.id.mobilecategoryRecyclerView);
        mobileArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCellphonesPhones", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Mobile mobileModel = new Mobile();
                            mobileModel.setId(id);
                            mobileModel.setName(name);
                            mobileModel.setAdded_by(added_by);
                            mobileModel.setCategory_id(category_id);
                            mobileModel.setSubcategory_id(subcategory_id);
                            mobileModel.setSubsubcategory_id(subsubcategory_id);
                            mobileModel.setBrand_id(brand_id);
                            mobileModel.setPhotos(photos);
                            mobileModel.setThumbnail_img(thumbnail_img);
                            mobileModel.setFeatured_img(featured_img);
                            mobileModel.setFlash_deal_img(flash_deal_img);
                            mobileModel.setVideo_provider(video_provider);
                            mobileModel.setVideo_link(video_link);
                            mobileModel.setTags(tags);
                            mobileModel.setDescription(description);
                            mobileModel.setUnit_price(unit_price);
                            mobileModel.setPurchase_price(purchase_price);
                            mobileModel.setChoice_options(choice_options);
                            mobileModel.setColors(colors);
                            mobileModel.setVariations(variations);
                            mobileModel.setTodays_deal(todays_deal);
                            mobileModel.setPublished(published);
                            mobileModel.setFeatured(featured);
                            mobileModel.setCurrent_stock(current_stock);
                            mobileModel.setUnit(unit);
                            mobileModel.setDiscount(discount);
                            mobileModel.setDiscount_type(discount_type);
                            mobileModel.setTax(tax);
                            mobileModel.setTax_type(tax_type);
                            mobileModel.setShipping_type(shipping_type);
                            mobileModel.setShipping_cost(shipping_cost);
                            mobileModel.setWeight(weight);
                            mobileModel.setParcel_size(parcel_size);
                            mobileModel.setNum_of_sale(num_of_sale);
                            mobileModel.setMeta_title(meta_title);
                            mobileModel.setMeta_description(meta_description);
                            mobileModel.setMeta_img(meta_img);
                            mobileModel.setPdf(pdf);
                            mobileModel.setSlug(slug);
                            mobileModel.setRating(rating);
                            mobileModel.setCreated_at(created_at);
                            mobileModel.setUpdated_at(updated_at);

                            mobileArrayList.add(mobileModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        mobilecategoryRecyclerView.setLayoutManager(layoutManager);

                        mobileProductGridAdapter = new MobileProductGridAdapter(context, mobileArrayList);
                        mobileProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedMobile = mobileArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedMobile.getName());
                                intent.putExtra("ITEMPRICE", selectedMobile.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedMobile.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedMobile.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedMobile.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        mobilecategoryRecyclerView.setAdapter(mobileProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
}
