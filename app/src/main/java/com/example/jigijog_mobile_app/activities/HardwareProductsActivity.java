package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.HardwareProductsAdapter;
import com.example.jigijog_mobile_app.adapters.HardwareProductsGridAdapter;
import com.example.jigijog_mobile_app.adapters.HealthProductAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Hardware;
import com.example.jigijog_mobile_app.models.Health;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class HardwareProductsActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private ImageView iv_hardwarecategoryBack, iv_hardwarecategoySearch,iv_hardwarecategoryCart,iv_hardwarecategoryMore,iv_hardwarecategoryGrid;
    private TextView tv_hardwarecategoryName;

    private String message;

    private ConstraintLayout emptyIndicator;

    private RecyclerView hardwarecategoryRecyclerView;
    private ArrayList<Hardware> hardwareArrayList;
    private HardwareProductsAdapter hardwareProductsAdapter;
    private HardwareProductsGridAdapter hardwareProductsGridAdapter;

    private Hardware selectedHardware;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hardware_products);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();
        if(message.equals("Hand Tool")){
            tv_hardwarecategoryName.setText(message);
            loadHardwareHandTool();
            iv_hardwarecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHardwareHandToolGrid();
                }
            });
        }else if(message.equals("Saws, Grinder, & Rotary Tools")){
            tv_hardwarecategoryName.setText(message);
            loadHardwareSawsGrinder();
            iv_hardwarecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHardwareSawsGrinderGrid();
                }
            });
        }else if(message.equals("Safety Equipment")){
            tv_hardwarecategoryName.setText(message);
            loadHardwareSafetyEquipment();
            iv_hardwarecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHardwareSafetyEquipmentGrid();
                }
            });
        }else if(message.equals("Others")){
            tv_hardwarecategoryName.setText(message);
            loadHardwareOthers();
            iv_hardwarecategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHardwareOthersGrid();
                }
            });
        }
    }

    private void initializeUI() {
        emptyIndicator = findViewById(R.id.view_Empty);
        iv_hardwarecategoryBack = findViewById(R.id.iv_hardwarecategoryBack);
        iv_hardwarecategoySearch = findViewById(R.id.iv_hardwarecategoySearch);
        iv_hardwarecategoryCart = findViewById(R.id.iv_hardwarecategoryCart);
        iv_hardwarecategoryMore = findViewById(R.id.iv_hardwarecategoryMore);
        iv_hardwarecategoryGrid = findViewById(R.id.iv_hardwarecategoryGrid);
        tv_hardwarecategoryName = findViewById(R.id.tv_hardwarecategoryName);
        iv_hardwarecategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_hardwarecategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    private void loadHardwareHandTool(){
        hardwarecategoryRecyclerView = findViewById(R.id.hardwarecategoryRecyclerView);
        hardwareArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHandTool", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Hardware hardwareModel = new Hardware();
                            hardwareModel.setId(id);
                            hardwareModel.setName(name);
                            hardwareModel.setAdded_by(added_by);
                            hardwareModel.setCategory_id(category_id);
                            hardwareModel.setSubcategory_id(subcategory_id);
                            hardwareModel.setSubsubcategory_id(subsubcategory_id);
                            hardwareModel.setBrand_id(brand_id);
                            hardwareModel.setPhotos(photos);
                            hardwareModel.setThumbnail_img(thumbnail_img);
                            hardwareModel.setFeatured_img(featured_img);
                            hardwareModel.setFlash_deal_img(flash_deal_img);
                            hardwareModel.setVideo_provider(video_provider);
                            hardwareModel.setVideo_link(video_link);
                            hardwareModel.setTags(tags);
                            hardwareModel.setDescription(description);
                            hardwareModel.setUnit_price(unit_price);
                            hardwareModel.setPurchase_price(purchase_price);
                            hardwareModel.setChoice_options(choice_options);
                            hardwareModel.setColors(colors);
                            hardwareModel.setVariations(variations);
                            hardwareModel.setTodays_deal(todays_deal);
                            hardwareModel.setPublished(published);
                            hardwareModel.setFeatured(featured);
                            hardwareModel.setCurrent_stock(current_stock);
                            hardwareModel.setUnit(unit);
                            hardwareModel.setDiscount(discount);
                            hardwareModel.setDiscount_type(discount_type);
                            hardwareModel.setTax(tax);
                            hardwareModel.setTax_type(tax_type);
                            hardwareModel.setShipping_type(shipping_type);
                            hardwareModel.setShipping_cost(shipping_cost);
                            hardwareModel.setWeight(weight);
                            hardwareModel.setParcel_size(parcel_size);
                            hardwareModel.setNum_of_sale(num_of_sale);
                            hardwareModel.setMeta_title(meta_title);
                            hardwareModel.setMeta_description(meta_description);
                            hardwareModel.setMeta_img(meta_img);
                            hardwareModel.setPdf(pdf);
                            hardwareModel.setSlug(slug);
                            hardwareModel.setRating(rating);
                            hardwareModel.setCreated_at(created_at);
                            hardwareModel.setUpdated_at(updated_at);

                            hardwareArrayList.add(hardwareModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        hardwarecategoryRecyclerView.setLayoutManager(layoutManager);

                        hardwareProductsAdapter = new HardwareProductsAdapter(context, hardwareArrayList);
                        hardwareProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHardware = hardwareArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHardware.getName());
                                intent.putExtra("ITEMPRICE", selectedHardware.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHardware.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHardware.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHardware.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        hardwarecategoryRecyclerView.setAdapter(hardwareProductsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHardwareSawsGrinder(){
        hardwarecategoryRecyclerView = findViewById(R.id.hardwarecategoryRecyclerView);
        hardwareArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllSawsGrindersAndRotaryTools", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Hardware hardwareModel = new Hardware();
                            hardwareModel.setId(id);
                            hardwareModel.setName(name);
                            hardwareModel.setAdded_by(added_by);
                            hardwareModel.setCategory_id(category_id);
                            hardwareModel.setSubcategory_id(subcategory_id);
                            hardwareModel.setSubsubcategory_id(subsubcategory_id);
                            hardwareModel.setBrand_id(brand_id);
                            hardwareModel.setPhotos(photos);
                            hardwareModel.setThumbnail_img(thumbnail_img);
                            hardwareModel.setFeatured_img(featured_img);
                            hardwareModel.setFlash_deal_img(flash_deal_img);
                            hardwareModel.setVideo_provider(video_provider);
                            hardwareModel.setVideo_link(video_link);
                            hardwareModel.setTags(tags);
                            hardwareModel.setDescription(description);
                            hardwareModel.setUnit_price(unit_price);
                            hardwareModel.setPurchase_price(purchase_price);
                            hardwareModel.setChoice_options(choice_options);
                            hardwareModel.setColors(colors);
                            hardwareModel.setVariations(variations);
                            hardwareModel.setTodays_deal(todays_deal);
                            hardwareModel.setPublished(published);
                            hardwareModel.setFeatured(featured);
                            hardwareModel.setCurrent_stock(current_stock);
                            hardwareModel.setUnit(unit);
                            hardwareModel.setDiscount(discount);
                            hardwareModel.setDiscount_type(discount_type);
                            hardwareModel.setTax(tax);
                            hardwareModel.setTax_type(tax_type);
                            hardwareModel.setShipping_type(shipping_type);
                            hardwareModel.setShipping_cost(shipping_cost);
                            hardwareModel.setWeight(weight);
                            hardwareModel.setParcel_size(parcel_size);
                            hardwareModel.setNum_of_sale(num_of_sale);
                            hardwareModel.setMeta_title(meta_title);
                            hardwareModel.setMeta_description(meta_description);
                            hardwareModel.setMeta_img(meta_img);
                            hardwareModel.setPdf(pdf);
                            hardwareModel.setSlug(slug);
                            hardwareModel.setRating(rating);
                            hardwareModel.setCreated_at(created_at);
                            hardwareModel.setUpdated_at(updated_at);

                            hardwareArrayList.add(hardwareModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        hardwarecategoryRecyclerView.setLayoutManager(layoutManager);

                        hardwareProductsAdapter = new HardwareProductsAdapter(context, hardwareArrayList);
                        hardwareProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHardware = hardwareArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHardware.getName());
                                intent.putExtra("ITEMPRICE", selectedHardware.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHardware.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHardware.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHardware.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        hardwarecategoryRecyclerView.setAdapter(hardwareProductsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHardwareSafetyEquipment(){
        hardwarecategoryRecyclerView = findViewById(R.id.hardwarecategoryRecyclerView);
        hardwareArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllSafetyEquipment", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Hardware hardwareModel = new Hardware();
                            hardwareModel.setId(id);
                            hardwareModel.setName(name);
                            hardwareModel.setAdded_by(added_by);
                            hardwareModel.setCategory_id(category_id);
                            hardwareModel.setSubcategory_id(subcategory_id);
                            hardwareModel.setSubsubcategory_id(subsubcategory_id);
                            hardwareModel.setBrand_id(brand_id);
                            hardwareModel.setPhotos(photos);
                            hardwareModel.setThumbnail_img(thumbnail_img);
                            hardwareModel.setFeatured_img(featured_img);
                            hardwareModel.setFlash_deal_img(flash_deal_img);
                            hardwareModel.setVideo_provider(video_provider);
                            hardwareModel.setVideo_link(video_link);
                            hardwareModel.setTags(tags);
                            hardwareModel.setDescription(description);
                            hardwareModel.setUnit_price(unit_price);
                            hardwareModel.setPurchase_price(purchase_price);
                            hardwareModel.setChoice_options(choice_options);
                            hardwareModel.setColors(colors);
                            hardwareModel.setVariations(variations);
                            hardwareModel.setTodays_deal(todays_deal);
                            hardwareModel.setPublished(published);
                            hardwareModel.setFeatured(featured);
                            hardwareModel.setCurrent_stock(current_stock);
                            hardwareModel.setUnit(unit);
                            hardwareModel.setDiscount(discount);
                            hardwareModel.setDiscount_type(discount_type);
                            hardwareModel.setTax(tax);
                            hardwareModel.setTax_type(tax_type);
                            hardwareModel.setShipping_type(shipping_type);
                            hardwareModel.setShipping_cost(shipping_cost);
                            hardwareModel.setWeight(weight);
                            hardwareModel.setParcel_size(parcel_size);
                            hardwareModel.setNum_of_sale(num_of_sale);
                            hardwareModel.setMeta_title(meta_title);
                            hardwareModel.setMeta_description(meta_description);
                            hardwareModel.setMeta_img(meta_img);
                            hardwareModel.setPdf(pdf);
                            hardwareModel.setSlug(slug);
                            hardwareModel.setRating(rating);
                            hardwareModel.setCreated_at(created_at);
                            hardwareModel.setUpdated_at(updated_at);

                            hardwareArrayList.add(hardwareModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        hardwarecategoryRecyclerView.setLayoutManager(layoutManager);

                        hardwareProductsAdapter = new HardwareProductsAdapter(context, hardwareArrayList);
                        hardwareProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHardware = hardwareArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHardware.getName());
                                intent.putExtra("ITEMPRICE", selectedHardware.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHardware.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHardware.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHardware.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        hardwarecategoryRecyclerView.setAdapter(hardwareProductsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHardwareOthers(){
        hardwarecategoryRecyclerView = findViewById(R.id.hardwarecategoryRecyclerView);
        hardwareArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHardwaresAndToolsOthers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Hardware hardwareModel = new Hardware();
                            hardwareModel.setId(id);
                            hardwareModel.setName(name);
                            hardwareModel.setAdded_by(added_by);
                            hardwareModel.setCategory_id(category_id);
                            hardwareModel.setSubcategory_id(subcategory_id);
                            hardwareModel.setSubsubcategory_id(subsubcategory_id);
                            hardwareModel.setBrand_id(brand_id);
                            hardwareModel.setPhotos(photos);
                            hardwareModel.setThumbnail_img(thumbnail_img);
                            hardwareModel.setFeatured_img(featured_img);
                            hardwareModel.setFlash_deal_img(flash_deal_img);
                            hardwareModel.setVideo_provider(video_provider);
                            hardwareModel.setVideo_link(video_link);
                            hardwareModel.setTags(tags);
                            hardwareModel.setDescription(description);
                            hardwareModel.setUnit_price(unit_price);
                            hardwareModel.setPurchase_price(purchase_price);
                            hardwareModel.setChoice_options(choice_options);
                            hardwareModel.setColors(colors);
                            hardwareModel.setVariations(variations);
                            hardwareModel.setTodays_deal(todays_deal);
                            hardwareModel.setPublished(published);
                            hardwareModel.setFeatured(featured);
                            hardwareModel.setCurrent_stock(current_stock);
                            hardwareModel.setUnit(unit);
                            hardwareModel.setDiscount(discount);
                            hardwareModel.setDiscount_type(discount_type);
                            hardwareModel.setTax(tax);
                            hardwareModel.setTax_type(tax_type);
                            hardwareModel.setShipping_type(shipping_type);
                            hardwareModel.setShipping_cost(shipping_cost);
                            hardwareModel.setWeight(weight);
                            hardwareModel.setParcel_size(parcel_size);
                            hardwareModel.setNum_of_sale(num_of_sale);
                            hardwareModel.setMeta_title(meta_title);
                            hardwareModel.setMeta_description(meta_description);
                            hardwareModel.setMeta_img(meta_img);
                            hardwareModel.setPdf(pdf);
                            hardwareModel.setSlug(slug);
                            hardwareModel.setRating(rating);
                            hardwareModel.setCreated_at(created_at);
                            hardwareModel.setUpdated_at(updated_at);

                            hardwareArrayList.add(hardwareModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        hardwarecategoryRecyclerView.setLayoutManager(layoutManager);

                        hardwareProductsAdapter = new HardwareProductsAdapter(context, hardwareArrayList);
                        hardwareProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHardware = hardwareArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHardware.getName());
                                intent.putExtra("ITEMPRICE", selectedHardware.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHardware.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHardware.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHardware.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        hardwarecategoryRecyclerView.setAdapter(hardwareProductsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadHardwareHandToolGrid(){
        hardwarecategoryRecyclerView = findViewById(R.id.hardwarecategoryRecyclerView);
        hardwareArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHandTool", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Hardware hardwareModel = new Hardware();
                            hardwareModel.setId(id);
                            hardwareModel.setName(name);
                            hardwareModel.setAdded_by(added_by);
                            hardwareModel.setCategory_id(category_id);
                            hardwareModel.setSubcategory_id(subcategory_id);
                            hardwareModel.setSubsubcategory_id(subsubcategory_id);
                            hardwareModel.setBrand_id(brand_id);
                            hardwareModel.setPhotos(photos);
                            hardwareModel.setThumbnail_img(thumbnail_img);
                            hardwareModel.setFeatured_img(featured_img);
                            hardwareModel.setFlash_deal_img(flash_deal_img);
                            hardwareModel.setVideo_provider(video_provider);
                            hardwareModel.setVideo_link(video_link);
                            hardwareModel.setTags(tags);
                            hardwareModel.setDescription(description);
                            hardwareModel.setUnit_price(unit_price);
                            hardwareModel.setPurchase_price(purchase_price);
                            hardwareModel.setChoice_options(choice_options);
                            hardwareModel.setColors(colors);
                            hardwareModel.setVariations(variations);
                            hardwareModel.setTodays_deal(todays_deal);
                            hardwareModel.setPublished(published);
                            hardwareModel.setFeatured(featured);
                            hardwareModel.setCurrent_stock(current_stock);
                            hardwareModel.setUnit(unit);
                            hardwareModel.setDiscount(discount);
                            hardwareModel.setDiscount_type(discount_type);
                            hardwareModel.setTax(tax);
                            hardwareModel.setTax_type(tax_type);
                            hardwareModel.setShipping_type(shipping_type);
                            hardwareModel.setShipping_cost(shipping_cost);
                            hardwareModel.setWeight(weight);
                            hardwareModel.setParcel_size(parcel_size);
                            hardwareModel.setNum_of_sale(num_of_sale);
                            hardwareModel.setMeta_title(meta_title);
                            hardwareModel.setMeta_description(meta_description);
                            hardwareModel.setMeta_img(meta_img);
                            hardwareModel.setPdf(pdf);
                            hardwareModel.setSlug(slug);
                            hardwareModel.setRating(rating);
                            hardwareModel.setCreated_at(created_at);
                            hardwareModel.setUpdated_at(updated_at);

                            hardwareArrayList.add(hardwareModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        hardwarecategoryRecyclerView.setLayoutManager(layoutManager);

                        hardwareProductsGridAdapter = new HardwareProductsGridAdapter(context, hardwareArrayList);
                        hardwareProductsGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHardware = hardwareArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHardware.getName());
                                intent.putExtra("ITEMPRICE", selectedHardware.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHardware.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHardware.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHardware.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        hardwarecategoryRecyclerView.setAdapter(hardwareProductsGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHardwareSawsGrinderGrid(){
        hardwarecategoryRecyclerView = findViewById(R.id.hardwarecategoryRecyclerView);
        hardwareArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllSawsGrindersAndRotaryTools", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Hardware hardwareModel = new Hardware();
                            hardwareModel.setId(id);
                            hardwareModel.setName(name);
                            hardwareModel.setAdded_by(added_by);
                            hardwareModel.setCategory_id(category_id);
                            hardwareModel.setSubcategory_id(subcategory_id);
                            hardwareModel.setSubsubcategory_id(subsubcategory_id);
                            hardwareModel.setBrand_id(brand_id);
                            hardwareModel.setPhotos(photos);
                            hardwareModel.setThumbnail_img(thumbnail_img);
                            hardwareModel.setFeatured_img(featured_img);
                            hardwareModel.setFlash_deal_img(flash_deal_img);
                            hardwareModel.setVideo_provider(video_provider);
                            hardwareModel.setVideo_link(video_link);
                            hardwareModel.setTags(tags);
                            hardwareModel.setDescription(description);
                            hardwareModel.setUnit_price(unit_price);
                            hardwareModel.setPurchase_price(purchase_price);
                            hardwareModel.setChoice_options(choice_options);
                            hardwareModel.setColors(colors);
                            hardwareModel.setVariations(variations);
                            hardwareModel.setTodays_deal(todays_deal);
                            hardwareModel.setPublished(published);
                            hardwareModel.setFeatured(featured);
                            hardwareModel.setCurrent_stock(current_stock);
                            hardwareModel.setUnit(unit);
                            hardwareModel.setDiscount(discount);
                            hardwareModel.setDiscount_type(discount_type);
                            hardwareModel.setTax(tax);
                            hardwareModel.setTax_type(tax_type);
                            hardwareModel.setShipping_type(shipping_type);
                            hardwareModel.setShipping_cost(shipping_cost);
                            hardwareModel.setWeight(weight);
                            hardwareModel.setParcel_size(parcel_size);
                            hardwareModel.setNum_of_sale(num_of_sale);
                            hardwareModel.setMeta_title(meta_title);
                            hardwareModel.setMeta_description(meta_description);
                            hardwareModel.setMeta_img(meta_img);
                            hardwareModel.setPdf(pdf);
                            hardwareModel.setSlug(slug);
                            hardwareModel.setRating(rating);
                            hardwareModel.setCreated_at(created_at);
                            hardwareModel.setUpdated_at(updated_at);

                            hardwareArrayList.add(hardwareModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        hardwarecategoryRecyclerView.setLayoutManager(layoutManager);

                        hardwareProductsGridAdapter = new HardwareProductsGridAdapter(context, hardwareArrayList);
                        hardwareProductsGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHardware = hardwareArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHardware.getName());
                                intent.putExtra("ITEMPRICE", selectedHardware.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHardware.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHardware.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHardware.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        hardwarecategoryRecyclerView.setAdapter(hardwareProductsGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHardwareSafetyEquipmentGrid(){
        hardwarecategoryRecyclerView = findViewById(R.id.hardwarecategoryRecyclerView);
        hardwareArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllSafetyEquipment", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Hardware hardwareModel = new Hardware();
                            hardwareModel.setId(id);
                            hardwareModel.setName(name);
                            hardwareModel.setAdded_by(added_by);
                            hardwareModel.setCategory_id(category_id);
                            hardwareModel.setSubcategory_id(subcategory_id);
                            hardwareModel.setSubsubcategory_id(subsubcategory_id);
                            hardwareModel.setBrand_id(brand_id);
                            hardwareModel.setPhotos(photos);
                            hardwareModel.setThumbnail_img(thumbnail_img);
                            hardwareModel.setFeatured_img(featured_img);
                            hardwareModel.setFlash_deal_img(flash_deal_img);
                            hardwareModel.setVideo_provider(video_provider);
                            hardwareModel.setVideo_link(video_link);
                            hardwareModel.setTags(tags);
                            hardwareModel.setDescription(description);
                            hardwareModel.setUnit_price(unit_price);
                            hardwareModel.setPurchase_price(purchase_price);
                            hardwareModel.setChoice_options(choice_options);
                            hardwareModel.setColors(colors);
                            hardwareModel.setVariations(variations);
                            hardwareModel.setTodays_deal(todays_deal);
                            hardwareModel.setPublished(published);
                            hardwareModel.setFeatured(featured);
                            hardwareModel.setCurrent_stock(current_stock);
                            hardwareModel.setUnit(unit);
                            hardwareModel.setDiscount(discount);
                            hardwareModel.setDiscount_type(discount_type);
                            hardwareModel.setTax(tax);
                            hardwareModel.setTax_type(tax_type);
                            hardwareModel.setShipping_type(shipping_type);
                            hardwareModel.setShipping_cost(shipping_cost);
                            hardwareModel.setWeight(weight);
                            hardwareModel.setParcel_size(parcel_size);
                            hardwareModel.setNum_of_sale(num_of_sale);
                            hardwareModel.setMeta_title(meta_title);
                            hardwareModel.setMeta_description(meta_description);
                            hardwareModel.setMeta_img(meta_img);
                            hardwareModel.setPdf(pdf);
                            hardwareModel.setSlug(slug);
                            hardwareModel.setRating(rating);
                            hardwareModel.setCreated_at(created_at);
                            hardwareModel.setUpdated_at(updated_at);

                            hardwareArrayList.add(hardwareModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        hardwarecategoryRecyclerView.setLayoutManager(layoutManager);

                        hardwareProductsGridAdapter = new HardwareProductsGridAdapter(context, hardwareArrayList);
                        hardwareProductsGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHardware = hardwareArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHardware.getName());
                                intent.putExtra("ITEMPRICE", selectedHardware.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHardware.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHardware.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHardware.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        hardwarecategoryRecyclerView.setAdapter(hardwareProductsGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHardwareOthersGrid(){
        hardwarecategoryRecyclerView = findViewById(R.id.hardwarecategoryRecyclerView);
        hardwareArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHardwaresAndToolsOthers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Hardware hardwareModel = new Hardware();
                            hardwareModel.setId(id);
                            hardwareModel.setName(name);
                            hardwareModel.setAdded_by(added_by);
                            hardwareModel.setCategory_id(category_id);
                            hardwareModel.setSubcategory_id(subcategory_id);
                            hardwareModel.setSubsubcategory_id(subsubcategory_id);
                            hardwareModel.setBrand_id(brand_id);
                            hardwareModel.setPhotos(photos);
                            hardwareModel.setThumbnail_img(thumbnail_img);
                            hardwareModel.setFeatured_img(featured_img);
                            hardwareModel.setFlash_deal_img(flash_deal_img);
                            hardwareModel.setVideo_provider(video_provider);
                            hardwareModel.setVideo_link(video_link);
                            hardwareModel.setTags(tags);
                            hardwareModel.setDescription(description);
                            hardwareModel.setUnit_price(unit_price);
                            hardwareModel.setPurchase_price(purchase_price);
                            hardwareModel.setChoice_options(choice_options);
                            hardwareModel.setColors(colors);
                            hardwareModel.setVariations(variations);
                            hardwareModel.setTodays_deal(todays_deal);
                            hardwareModel.setPublished(published);
                            hardwareModel.setFeatured(featured);
                            hardwareModel.setCurrent_stock(current_stock);
                            hardwareModel.setUnit(unit);
                            hardwareModel.setDiscount(discount);
                            hardwareModel.setDiscount_type(discount_type);
                            hardwareModel.setTax(tax);
                            hardwareModel.setTax_type(tax_type);
                            hardwareModel.setShipping_type(shipping_type);
                            hardwareModel.setShipping_cost(shipping_cost);
                            hardwareModel.setWeight(weight);
                            hardwareModel.setParcel_size(parcel_size);
                            hardwareModel.setNum_of_sale(num_of_sale);
                            hardwareModel.setMeta_title(meta_title);
                            hardwareModel.setMeta_description(meta_description);
                            hardwareModel.setMeta_img(meta_img);
                            hardwareModel.setPdf(pdf);
                            hardwareModel.setSlug(slug);
                            hardwareModel.setRating(rating);
                            hardwareModel.setCreated_at(created_at);
                            hardwareModel.setUpdated_at(updated_at);

                            hardwareArrayList.add(hardwareModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        hardwarecategoryRecyclerView.setLayoutManager(layoutManager);

                        hardwareProductsGridAdapter = new HardwareProductsGridAdapter(context, hardwareArrayList);
                        hardwareProductsGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHardware = hardwareArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHardware.getName());
                                intent.putExtra("ITEMPRICE", selectedHardware.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHardware.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHardware.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHardware.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        hardwarecategoryRecyclerView.setAdapter(hardwareProductsGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
}
