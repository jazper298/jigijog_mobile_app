package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jigijog_mobile_app.HttpProvider;
import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.models.UserCustomer;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.ProgressPopup;
import com.example.jigijog_mobile_app.utils.Tools;
import com.example.jigijog_mobile_app.utils.UserRole;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity {

    private View view;
    private Context context;

    private EditText et_LoginEmail, et_LoginPassword;
    private Button btn_signin;

    private TextView sign_up;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
            Tools.setSystemBarColor(this, R.color.deep_orange_300);
            Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI () {
        et_LoginEmail = findViewById(R.id.et_LoginEmail);
        et_LoginPassword = findViewById(R.id.et_LoginPassword);
        btn_signin = findViewById(R.id.btn_signin);
        sign_up = findViewById(R.id.sign_up);
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fieldsAreEmpty())
                    Toasty.warning(context, "Complete the fields.").show();
                else {
                    if (!et_LoginEmail.getText().toString().contains("@"))
                        Toasty.warning(context, "Invalid email.").show();
                    else {
                        loginUser();
                    }
                }
            }
        });
    }
    private boolean fieldsAreEmpty() {
        if (et_LoginEmail.getText().toString().isEmpty() &&
                et_LoginPassword.getText().toString().isEmpty())
            return true;
        else
            return false;
    }

    private void loginUser(){
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("username", et_LoginEmail.getText().toString());
        params.put("password", et_LoginPassword.getText().toString());

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();

                String str = new String(responseBody, StandardCharsets.UTF_8);
                Debugger.logD("str " + str);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String id = jsonObject.getString("id");
                        String provider_id = jsonObject.getString("provider_id");
                        String user_type = jsonObject.getString("user_type");
                        String name = jsonObject.getString("name");
                        String email = jsonObject.getString("email");
                        String referred_by = jsonObject.getString("referred_by");
                        //String refer_pointsref_id_status = jsonObject.getString("refer_pointsref_id_status");
                        String email_verified_at = jsonObject.getString("email_verified_at");
                        String password = jsonObject.getString("password");
                    String remember_token = jsonObject.getString("remember_token");
                        String avatar = jsonObject.getString("avatar");
                        String avatar_original = jsonObject.getString("avatar_original");
                        String address = jsonObject.getString("address");
                        String country = jsonObject.getString("country");
                        String city = jsonObject.getString("city");
                        String barangay = jsonObject.getString("barangay");
                        String landmark = jsonObject.getString("landmark");
                        String postal_code = jsonObject.getString("postal_code");
                        String phone = jsonObject.getString("phone");
                        String bank_name = jsonObject.getString("bank_name");
                        String account_name = jsonObject.getString("account_name");
                        String account_number = jsonObject.getString("account_number");
                        String balance = jsonObject.getString("balance");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");

                    UserCustomer userCustomer = new UserCustomer();
                    userCustomer.setId(id);
                        userCustomer.setProvider_id(provider_id);
                        userCustomer.setUser_type(user_type);
                        userCustomer.setName(name);
                        userCustomer.setEmail(email);
                        userCustomer.setReferred_by(referred_by);
                        //userCustomer.setRefer_pointsref_id_status(refer_pointsref_id_status);
                        userCustomer.setEmail_verified_at(email_verified_at);
                        userCustomer.setPassword(password);
                    userCustomer.setRemember_token(remember_token);
                        userCustomer.setAvatar(avatar);
                        userCustomer.setAvatar_original(avatar_original);
                        userCustomer.setAddress(address);
                        userCustomer.setCountry(country);
                        userCustomer.setCity(city);
                        userCustomer.setBarangay(barangay);
                        userCustomer.setLandmark(landmark);
                        userCustomer.setPostal_code(postal_code);
                        userCustomer.setPhone(phone);
                        userCustomer.setBank_name(bank_name);
                        userCustomer.setAccount_name(account_name);
                        userCustomer.setAccount_number(account_number);
                        userCustomer.setBalance(balance);
                        userCustomer.setCreated_at(created_at);
                        userCustomer.setUpdated_at(updated_at);

                    //getUserDetails(userCustomer);
                    UserRole userRole = new UserRole();
                    userRole.setUserRole(UserRole.Customer());
                    userRole.saveRole(context);

                    userCustomer.saveUserSession(context);
                    Toasty.success(context, "Success.").show();
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra("ROLE_ID", "customer");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

    private void getUserDetails(final UserCustomer userCustomer){

        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("id", userCustomer.getId());

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();
                try {
                    String str = new String(responseBody, StandardCharsets.UTF_8);
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Debugger.logD(str);
                    String id = jsonObject.getString("id");
                    String provider_id = jsonObject.getString("provider_id");
                    String user_type = jsonObject.getString("user_type");
                    String name = jsonObject.getString("name");
                    String email = jsonObject.getString("email");
                    String referred_by = jsonObject.getString("referred_by");
                    String refer_pointsref_id_status = jsonObject.getString("refer_pointsref_id_status");
                    String email_verified_at = jsonObject.getString("email_verified_at");
                    String password = jsonObject.getString("password");
                    String remember_token = jsonObject.getString("remember_token");
                    String avatar = jsonObject.getString("avatar");
                    String avatar_original = jsonObject.getString("avatar_original");
                    String address = jsonObject.getString("address");
                    String country = jsonObject.getString("country");
                    String city = jsonObject.getString("city");
                    String barangay = jsonObject.getString("barangay");
                    String landmark = jsonObject.getString("landmark");
                    String postal_code = jsonObject.getString("postal_code");
                    String phone = jsonObject.getString("phone");
                    String bank_name = jsonObject.getString("bank_name");
                    String account_name = jsonObject.getString("account_name");
                    String account_number = jsonObject.getString("account_number");
                    String balance = jsonObject.getString("balance");
                    String created_at = jsonObject.getString("created_at");
                    String updated_at = jsonObject.getString("updated_at");

                    UserCustomer userCustomer = new UserCustomer();
                    userCustomer.setId(id);
                    userCustomer.setProvider_id(provider_id);
                    userCustomer.setUser_type(user_type);
                    userCustomer.setName(name);
                    userCustomer.setEmail(email);
                    userCustomer.setReferred_by(referred_by);
                    userCustomer.setRefer_pointsref_id_status(refer_pointsref_id_status);
                    userCustomer.setEmail_verified_at(email_verified_at);
                    userCustomer.setPassword(password);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setAvatar(avatar);
                    userCustomer.setAvatar_original(avatar_original);
                    userCustomer.setAddress(address);
                    userCustomer.setCountry(country);
                    userCustomer.setCity(city);
                    userCustomer.setBarangay(barangay);
                    userCustomer.setLandmark(landmark);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setPostal_code(postal_code);
                    userCustomer.setPhone(phone);
                    userCustomer.setBank_name(bank_name);
                    userCustomer.setAccount_name(account_name);
                    userCustomer.setAccount_number(account_number);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setBalance(balance);
                    userCustomer.setCreated_at(created_at);
                    userCustomer.setUpdated_at(updated_at);

                    UserRole userRole = new UserRole();
                    userRole.setUserRole(UserRole.Customer());
                    userRole.saveRole(context);

                    userCustomer.saveUserSession(context);
                    Toasty.success(context, "Success.").show();
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra("ROLE_ID", "customer");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }
}
