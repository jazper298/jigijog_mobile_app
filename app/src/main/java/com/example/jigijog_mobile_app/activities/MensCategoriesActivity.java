package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.AssortedAdapter;
import com.example.jigijog_mobile_app.adapters.HotPicksAdapter;
import com.example.jigijog_mobile_app.adapters.MensProductAdapter;
import com.example.jigijog_mobile_app.adapters.MensProductGridAdapter;
import com.example.jigijog_mobile_app.fragments.HomeFragment;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Assorted;
import com.example.jigijog_mobile_app.models.HotPicks;
import com.example.jigijog_mobile_app.models.Men;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MensCategoriesActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private String message;

    private ImageView iv_menscategoryBack, iv_menscategoryGrid,iv_menscategoryMore,iv_menscategoryCart,iv_menscategoySearch;
    private TextView tv_menscategoryName;

    private ConstraintLayout emptyIndicator;


    private RecyclerView menscategoryRecyclerView;
    private ArrayList<Men> menArrayList;
    private MensProductAdapter mensProductAdapter;
    private MensProductGridAdapter mensProductGridAdapter;

    private SwipeRefreshLayout swipeRefreshLayout;

    private FragmentManager fragmentManager;

    private Men selectedMen = new Men();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mens_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();


        if(message.equals("Top")) {
            tv_menscategoryName.setText("Men's " + message);
            loadMensCategoryTop();
            iv_menscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMensCategoryTopGrid();
                }
            });
        }else if (message.equals("Bottom")){
            tv_menscategoryName.setText("Men's " + message);
            loadMensCategoryBottom();
            iv_menscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMensCategoryBottomGrid();
                }
            });
        }else if (message.equals("Footwear")){
            tv_menscategoryName.setText("Men's " + message);
            loadMensCategoryFootwear();
            iv_menscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMensCategoryFootwearGrid();
                }
            });
        }else if (message.equals("Sets & Accessories")){
            tv_menscategoryName.setText("Men's " + message);
            loadMensCategoryAccessories();
            iv_menscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMensCategoryAccessoriesGrid();
                }
            });
        }

    }

    private void initializeUI () {
        iv_menscategoryBack = findViewById(R.id.iv_menscategoryBack);
        tv_menscategoryName = findViewById(R.id.tv_menscategoryName);
        iv_menscategoryGrid = findViewById(R.id.iv_menscategoryGrid);
        iv_menscategoryMore = findViewById(R.id.iv_menscategoryMore);
        iv_menscategoySearch = findViewById(R.id.iv_menscategoySearch);
        iv_menscategoryCart = findViewById(R.id.iv_menscategoryCart);
        swipeRefreshLayout = findViewById(R.id.swipe_Home);
        swipeRefreshLayout.setRefreshing(false);

        iv_menscategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_menscategoySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        iv_menscategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_menscategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();

            }
        });


    }

    private void loadMensCategoryTop() {
        swipeRefreshLayout.setRefreshing(true);
        menscategoryRecyclerView = findViewById(R.id.menscategoryRecyclerView);
        menArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllMensTop", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Men menModel = new Men();
                        menModel.setId(id);
                        menModel.setName(name);
                        menModel.setAdded_by(added_by);
                        menModel.setCategory_id(category_id);
                        menModel.setSubcategory_id(subcategory_id);
                        menModel.setSubsubcategory_id(subsubcategory_id);
                        menModel.setBrand_id(brand_id);
                        menModel.setPhotos(photos);
                        menModel.setThumbnail_img(thumbnail_img);
                        menModel.setFeatured_img(featured_img);
                        menModel.setFlash_deal_img(flash_deal_img);
                        menModel.setVideo_provider(video_provider);
                        menModel.setVideo_link(video_link);
                        menModel.setTags(tags);
                        menModel.setDescription(description);
                        menModel.setUnit_price(unit_price);
                        menModel.setPurchase_price(purchase_price);
                        menModel.setChoice_options(choice_options);
                        menModel.setColors(colors);
                        menModel.setVariations(variations);
                        menModel.setTodays_deal(todays_deal);
                        menModel.setPublished(published);
                        menModel.setFeatured(featured);
                        menModel.setCurrent_stock(current_stock);
                        menModel.setUnit(unit);
                        menModel.setDiscount(discount);
                        menModel.setDiscount_type(discount_type);
                        menModel.setTax(tax);
                        menModel.setTax_type(tax_type);
                        menModel.setShipping_type(shipping_type);
                        menModel.setShipping_cost(shipping_cost);
                        menModel.setWeight(weight);
                        menModel.setParcel_size(parcel_size);
                        menModel.setNum_of_sale(num_of_sale);
                        menModel.setMeta_title(meta_title);
                        menModel.setMeta_description(meta_description);
                        menModel.setMeta_img(meta_img);
                        menModel.setPdf(pdf);
                        menModel.setSlug(slug);
                        menModel.setRating(rating);
                        menModel.setCreated_at(created_at);
                        menModel.setUpdated_at(updated_at);

                        menArrayList.add(menModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    menscategoryRecyclerView.setLayoutManager(layoutManager);

                    mensProductAdapter = new MensProductAdapter(context, menArrayList);
                    mensProductAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedMen = menArrayList.get(position);

//                Bundle extras = new Bundle();
//                Intent intent = new Intent(context, ProductViewActivity.class);
//                intent.putExtra("ITEMNAME", selectedMen.getMenItemName());
//                intent.putExtra("ITEMPRICE", selectedMen.getMenPrice());
//                intent.putExtra("ITEMDISPRICE", selectedMen.getMenDisPrice());
//                intent.putExtra("ITEMIMAGE",selectedMen.getMenImage());
//                intent.putExtras(extras);
//                startActivity(intent);
                        }
                    });
                    menscategoryRecyclerView.setAdapter(mensProductAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadMensCategoryBottom() {
        swipeRefreshLayout.setRefreshing(true);
        menscategoryRecyclerView = findViewById(R.id.menscategoryRecyclerView);
        menArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllMensBottom", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Men menModel = new Men();
                        menModel.setId(id);
                        menModel.setName(name);
                        menModel.setAdded_by(added_by);
                        menModel.setCategory_id(category_id);
                        menModel.setSubcategory_id(subcategory_id);
                        menModel.setSubsubcategory_id(subsubcategory_id);
                        menModel.setBrand_id(brand_id);
                        menModel.setPhotos(photos);
                        menModel.setThumbnail_img(thumbnail_img);
                        menModel.setFeatured_img(featured_img);
                        menModel.setFlash_deal_img(flash_deal_img);
                        menModel.setVideo_provider(video_provider);
                        menModel.setVideo_link(video_link);
                        menModel.setTags(tags);
                        menModel.setDescription(description);
                        menModel.setUnit_price(unit_price);
                        menModel.setPurchase_price(purchase_price);
                        menModel.setChoice_options(choice_options);
                        menModel.setColors(colors);
                        menModel.setVariations(variations);
                        menModel.setTodays_deal(todays_deal);
                        menModel.setPublished(published);
                        menModel.setFeatured(featured);
                        menModel.setCurrent_stock(current_stock);
                        menModel.setUnit(unit);
                        menModel.setDiscount(discount);
                        menModel.setDiscount_type(discount_type);
                        menModel.setTax(tax);
                        menModel.setTax_type(tax_type);
                        menModel.setShipping_type(shipping_type);
                        menModel.setShipping_cost(shipping_cost);
                        menModel.setWeight(weight);
                        menModel.setParcel_size(parcel_size);
                        menModel.setNum_of_sale(num_of_sale);
                        menModel.setMeta_title(meta_title);
                        menModel.setMeta_description(meta_description);
                        menModel.setMeta_img(meta_img);
                        menModel.setPdf(pdf);
                        menModel.setSlug(slug);
                        menModel.setRating(rating);
                        menModel.setCreated_at(created_at);
                        menModel.setUpdated_at(updated_at);

                        menArrayList.add(menModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    menscategoryRecyclerView.setLayoutManager(layoutManager);

                    mensProductAdapter = new MensProductAdapter(context, menArrayList);
                    mensProductAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedMen = menArrayList.get(position);

//                Bundle extras = new Bundle();
//                Intent intent = new Intent(context, ProductViewActivity.class);
//                intent.putExtra("ITEMNAME", selectedMen.getMenItemName());
//                intent.putExtra("ITEMPRICE", selectedMen.getMenPrice());
//                intent.putExtra("ITEMDISPRICE", selectedMen.getMenDisPrice());
//                intent.putExtra("ITEMIMAGE",selectedMen.getMenImage());
//                intent.putExtras(extras);
//                startActivity(intent);
                        }
                    });
                    menscategoryRecyclerView.setAdapter(mensProductAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadMensCategoryFootwear() {
        swipeRefreshLayout.setRefreshing(true);
        menscategoryRecyclerView = findViewById(R.id.menscategoryRecyclerView);
        menArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllMensFootwear", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Men menModel = new Men();
                        menModel.setId(id);
                        menModel.setName(name);
                        menModel.setAdded_by(added_by);
                        menModel.setCategory_id(category_id);
                        menModel.setSubcategory_id(subcategory_id);
                        menModel.setSubsubcategory_id(subsubcategory_id);
                        menModel.setBrand_id(brand_id);
                        menModel.setPhotos(photos);
                        menModel.setThumbnail_img(thumbnail_img);
                        menModel.setFeatured_img(featured_img);
                        menModel.setFlash_deal_img(flash_deal_img);
                        menModel.setVideo_provider(video_provider);
                        menModel.setVideo_link(video_link);
                        menModel.setTags(tags);
                        menModel.setDescription(description);
                        menModel.setUnit_price(unit_price);
                        menModel.setPurchase_price(purchase_price);
                        menModel.setChoice_options(choice_options);
                        menModel.setColors(colors);
                        menModel.setVariations(variations);
                        menModel.setTodays_deal(todays_deal);
                        menModel.setPublished(published);
                        menModel.setFeatured(featured);
                        menModel.setCurrent_stock(current_stock);
                        menModel.setUnit(unit);
                        menModel.setDiscount(discount);
                        menModel.setDiscount_type(discount_type);
                        menModel.setTax(tax);
                        menModel.setTax_type(tax_type);
                        menModel.setShipping_type(shipping_type);
                        menModel.setShipping_cost(shipping_cost);
                        menModel.setWeight(weight);
                        menModel.setParcel_size(parcel_size);
                        menModel.setNum_of_sale(num_of_sale);
                        menModel.setMeta_title(meta_title);
                        menModel.setMeta_description(meta_description);
                        menModel.setMeta_img(meta_img);
                        menModel.setPdf(pdf);
                        menModel.setSlug(slug);
                        menModel.setRating(rating);
                        menModel.setCreated_at(created_at);
                        menModel.setUpdated_at(updated_at);

                        menArrayList.add(menModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    menscategoryRecyclerView.setLayoutManager(layoutManager);

                    mensProductAdapter = new MensProductAdapter(context, menArrayList);
                    mensProductAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedMen = menArrayList.get(position);

//                Bundle extras = new Bundle();
//                Intent intent = new Intent(context, ProductViewActivity.class);
//                intent.putExtra("ITEMNAME", selectedMen.getMenItemName());
//                intent.putExtra("ITEMPRICE", selectedMen.getMenPrice());
//                intent.putExtra("ITEMDISPRICE", selectedMen.getMenDisPrice());
//                intent.putExtra("ITEMIMAGE",selectedMen.getMenImage());
//                intent.putExtras(extras);
//                startActivity(intent);
                        }
                    });
                    menscategoryRecyclerView.setAdapter(mensProductAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });


    }

    private void loadMensCategoryAccessories() {
        swipeRefreshLayout.setRefreshing(true);
        menscategoryRecyclerView = findViewById(R.id.menscategoryRecyclerView);
        menArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllMensSetsAccessories", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Men menModel = new Men();
                        menModel.setId(id);
                        menModel.setName(name);
                        menModel.setAdded_by(added_by);
                        menModel.setCategory_id(category_id);
                        menModel.setSubcategory_id(subcategory_id);
                        menModel.setSubsubcategory_id(subsubcategory_id);
                        menModel.setBrand_id(brand_id);
                        menModel.setPhotos(photos);
                        menModel.setThumbnail_img(thumbnail_img);
                        menModel.setFeatured_img(featured_img);
                        menModel.setFlash_deal_img(flash_deal_img);
                        menModel.setVideo_provider(video_provider);
                        menModel.setVideo_link(video_link);
                        menModel.setTags(tags);
                        menModel.setDescription(description);
                        menModel.setUnit_price(unit_price);
                        menModel.setPurchase_price(purchase_price);
                        menModel.setChoice_options(choice_options);
                        menModel.setColors(colors);
                        menModel.setVariations(variations);
                        menModel.setTodays_deal(todays_deal);
                        menModel.setPublished(published);
                        menModel.setFeatured(featured);
                        menModel.setCurrent_stock(current_stock);
                        menModel.setUnit(unit);
                        menModel.setDiscount(discount);
                        menModel.setDiscount_type(discount_type);
                        menModel.setTax(tax);
                        menModel.setTax_type(tax_type);
                        menModel.setShipping_type(shipping_type);
                        menModel.setShipping_cost(shipping_cost);
                        menModel.setWeight(weight);
                        menModel.setParcel_size(parcel_size);
                        menModel.setNum_of_sale(num_of_sale);
                        menModel.setMeta_title(meta_title);
                        menModel.setMeta_description(meta_description);
                        menModel.setMeta_img(meta_img);
                        menModel.setPdf(pdf);
                        menModel.setSlug(slug);
                        menModel.setRating(rating);
                        menModel.setCreated_at(created_at);
                        menModel.setUpdated_at(updated_at);

                        menArrayList.add(menModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    menscategoryRecyclerView.setLayoutManager(layoutManager);

                    mensProductAdapter = new MensProductAdapter(context, menArrayList);
                    mensProductAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedMen = menArrayList.get(position);
                        }
                    });
                    menscategoryRecyclerView.setAdapter(mensProductAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadMensCategoryTopGrid() {
        swipeRefreshLayout.setRefreshing(true);
        menscategoryRecyclerView = findViewById(R.id.menscategoryRecyclerView);
        menArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllMensTop", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Men menModel = new Men();
                        menModel.setId(id);
                        menModel.setName(name);
                        menModel.setAdded_by(added_by);
                        menModel.setCategory_id(category_id);
                        menModel.setSubcategory_id(subcategory_id);
                        menModel.setSubsubcategory_id(subsubcategory_id);
                        menModel.setBrand_id(brand_id);
                        menModel.setPhotos(photos);
                        menModel.setThumbnail_img(thumbnail_img);
                        menModel.setFeatured_img(featured_img);
                        menModel.setFlash_deal_img(flash_deal_img);
                        menModel.setVideo_provider(video_provider);
                        menModel.setVideo_link(video_link);
                        menModel.setTags(tags);
                        menModel.setDescription(description);
                        menModel.setUnit_price(unit_price);
                        menModel.setPurchase_price(purchase_price);
                        menModel.setChoice_options(choice_options);
                        menModel.setColors(colors);
                        menModel.setVariations(variations);
                        menModel.setTodays_deal(todays_deal);
                        menModel.setPublished(published);
                        menModel.setFeatured(featured);
                        menModel.setCurrent_stock(current_stock);
                        menModel.setUnit(unit);
                        menModel.setDiscount(discount);
                        menModel.setDiscount_type(discount_type);
                        menModel.setTax(tax);
                        menModel.setTax_type(tax_type);
                        menModel.setShipping_type(shipping_type);
                        menModel.setShipping_cost(shipping_cost);
                        menModel.setWeight(weight);
                        menModel.setParcel_size(parcel_size);
                        menModel.setNum_of_sale(num_of_sale);
                        menModel.setMeta_title(meta_title);
                        menModel.setMeta_description(meta_description);
                        menModel.setMeta_img(meta_img);
                        menModel.setPdf(pdf);
                        menModel.setSlug(slug);
                        menModel.setRating(rating);
                        menModel.setCreated_at(created_at);
                        menModel.setUpdated_at(updated_at);

                        menArrayList.add(menModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                    menscategoryRecyclerView.setLayoutManager(layoutManager);

                    mensProductGridAdapter = new MensProductGridAdapter(context, menArrayList);
                    mensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedMen = menArrayList.get(position);

//                            Bundle extras = new Bundle();
//                            Intent intent = new Intent(context, ProductViewActivity.class);
//                            intent.putExtra("ITEMNAME", selectedMen.getMenItemName());
//                            intent.putExtra("ITEMPRICE", selectedMen.getMenPrice());
//                            intent.putExtra("ITEMDISPRICE", selectedMen.getMenDisPrice());
//                            intent.putExtra("ITEMIMAGE",selectedMen.getMenImage());
//                            intent.putExtras(extras);
//                            startActivity(intent);
                        }
                    });
                    menscategoryRecyclerView.setAdapter(mensProductGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadMensCategoryBottomGrid() {
        swipeRefreshLayout.setRefreshing(true);
        menscategoryRecyclerView = findViewById(R.id.menscategoryRecyclerView);
        menArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllMensBottom", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Men menModel = new Men();
                        menModel.setId(id);
                        menModel.setName(name);
                        menModel.setAdded_by(added_by);
                        menModel.setCategory_id(category_id);
                        menModel.setSubcategory_id(subcategory_id);
                        menModel.setSubsubcategory_id(subsubcategory_id);
                        menModel.setBrand_id(brand_id);
                        menModel.setPhotos(photos);
                        menModel.setThumbnail_img(thumbnail_img);
                        menModel.setFeatured_img(featured_img);
                        menModel.setFlash_deal_img(flash_deal_img);
                        menModel.setVideo_provider(video_provider);
                        menModel.setVideo_link(video_link);
                        menModel.setTags(tags);
                        menModel.setDescription(description);
                        menModel.setUnit_price(unit_price);
                        menModel.setPurchase_price(purchase_price);
                        menModel.setChoice_options(choice_options);
                        menModel.setColors(colors);
                        menModel.setVariations(variations);
                        menModel.setTodays_deal(todays_deal);
                        menModel.setPublished(published);
                        menModel.setFeatured(featured);
                        menModel.setCurrent_stock(current_stock);
                        menModel.setUnit(unit);
                        menModel.setDiscount(discount);
                        menModel.setDiscount_type(discount_type);
                        menModel.setTax(tax);
                        menModel.setTax_type(tax_type);
                        menModel.setShipping_type(shipping_type);
                        menModel.setShipping_cost(shipping_cost);
                        menModel.setWeight(weight);
                        menModel.setParcel_size(parcel_size);
                        menModel.setNum_of_sale(num_of_sale);
                        menModel.setMeta_title(meta_title);
                        menModel.setMeta_description(meta_description);
                        menModel.setMeta_img(meta_img);
                        menModel.setPdf(pdf);
                        menModel.setSlug(slug);
                        menModel.setRating(rating);
                        menModel.setCreated_at(created_at);
                        menModel.setUpdated_at(updated_at);

                        menArrayList.add(menModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                    menscategoryRecyclerView.setLayoutManager(layoutManager);

                    mensProductGridAdapter = new MensProductGridAdapter(context, menArrayList);
                    mensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedMen = menArrayList.get(position);

                        }
                    });
                    menscategoryRecyclerView.setAdapter(mensProductGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadMensCategoryFootwearGrid() {
        swipeRefreshLayout.setRefreshing(true);
        menscategoryRecyclerView = findViewById(R.id.menscategoryRecyclerView);
        menArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllMensFootwear", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Men menModel = new Men();
                        menModel.setId(id);
                        menModel.setName(name);
                        menModel.setAdded_by(added_by);
                        menModel.setCategory_id(category_id);
                        menModel.setSubcategory_id(subcategory_id);
                        menModel.setSubsubcategory_id(subsubcategory_id);
                        menModel.setBrand_id(brand_id);
                        menModel.setPhotos(photos);
                        menModel.setThumbnail_img(thumbnail_img);
                        menModel.setFeatured_img(featured_img);
                        menModel.setFlash_deal_img(flash_deal_img);
                        menModel.setVideo_provider(video_provider);
                        menModel.setVideo_link(video_link);
                        menModel.setTags(tags);
                        menModel.setDescription(description);
                        menModel.setUnit_price(unit_price);
                        menModel.setPurchase_price(purchase_price);
                        menModel.setChoice_options(choice_options);
                        menModel.setColors(colors);
                        menModel.setVariations(variations);
                        menModel.setTodays_deal(todays_deal);
                        menModel.setPublished(published);
                        menModel.setFeatured(featured);
                        menModel.setCurrent_stock(current_stock);
                        menModel.setUnit(unit);
                        menModel.setDiscount(discount);
                        menModel.setDiscount_type(discount_type);
                        menModel.setTax(tax);
                        menModel.setTax_type(tax_type);
                        menModel.setShipping_type(shipping_type);
                        menModel.setShipping_cost(shipping_cost);
                        menModel.setWeight(weight);
                        menModel.setParcel_size(parcel_size);
                        menModel.setNum_of_sale(num_of_sale);
                        menModel.setMeta_title(meta_title);
                        menModel.setMeta_description(meta_description);
                        menModel.setMeta_img(meta_img);
                        menModel.setPdf(pdf);
                        menModel.setSlug(slug);
                        menModel.setRating(rating);
                        menModel.setCreated_at(created_at);
                        menModel.setUpdated_at(updated_at);

                        menArrayList.add(menModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                    menscategoryRecyclerView.setLayoutManager(layoutManager);

                    mensProductGridAdapter = new MensProductGridAdapter(context, menArrayList);
                    mensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedMen = menArrayList.get(position);


                        }
                    });
                    menscategoryRecyclerView.setAdapter(mensProductGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadMensCategoryAccessoriesGrid() {
        swipeRefreshLayout.setRefreshing(true);
        menscategoryRecyclerView = findViewById(R.id.menscategoryRecyclerView);
        menArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllMensSetsAccessories", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Men menModel = new Men();
                        menModel.setId(id);
                        menModel.setName(name);
                        menModel.setAdded_by(added_by);
                        menModel.setCategory_id(category_id);
                        menModel.setSubcategory_id(subcategory_id);
                        menModel.setSubsubcategory_id(subsubcategory_id);
                        menModel.setBrand_id(brand_id);
                        menModel.setPhotos(photos);
                        menModel.setThumbnail_img(thumbnail_img);
                        menModel.setFeatured_img(featured_img);
                        menModel.setFlash_deal_img(flash_deal_img);
                        menModel.setVideo_provider(video_provider);
                        menModel.setVideo_link(video_link);
                        menModel.setTags(tags);
                        menModel.setDescription(description);
                        menModel.setUnit_price(unit_price);
                        menModel.setPurchase_price(purchase_price);
                        menModel.setChoice_options(choice_options);
                        menModel.setColors(colors);
                        menModel.setVariations(variations);
                        menModel.setTodays_deal(todays_deal);
                        menModel.setPublished(published);
                        menModel.setFeatured(featured);
                        menModel.setCurrent_stock(current_stock);
                        menModel.setUnit(unit);
                        menModel.setDiscount(discount);
                        menModel.setDiscount_type(discount_type);
                        menModel.setTax(tax);
                        menModel.setTax_type(tax_type);
                        menModel.setShipping_type(shipping_type);
                        menModel.setShipping_cost(shipping_cost);
                        menModel.setWeight(weight);
                        menModel.setParcel_size(parcel_size);
                        menModel.setNum_of_sale(num_of_sale);
                        menModel.setMeta_title(meta_title);
                        menModel.setMeta_description(meta_description);
                        menModel.setMeta_img(meta_img);
                        menModel.setPdf(pdf);
                        menModel.setSlug(slug);
                        menModel.setRating(rating);
                        menModel.setCreated_at(created_at);
                        menModel.setUpdated_at(updated_at);

                        menArrayList.add(menModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                    menscategoryRecyclerView.setLayoutManager(layoutManager);

                    mensProductGridAdapter = new MensProductGridAdapter(context, menArrayList);
                    mensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedMen = menArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedMen.getName());
                            intent.putExtra("ITEMPRICE", selectedMen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedMen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedMen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    menscategoryRecyclerView.setAdapter(mensProductGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    public void openHomeFragment(){
        HomeFragment homeFragment = new HomeFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.homeFragment,homeFragment,homeFragment.getTag()).commit();
    }
    public void openMyAccountFragment(){
        HomeFragment homeFragment = new HomeFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.messageFragment,homeFragment,homeFragment.getTag()).commit();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
    }

}
