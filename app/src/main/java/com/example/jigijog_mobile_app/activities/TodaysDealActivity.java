package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.utils.Tools;

public class TodaysDealActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private ImageView iv_todaysdealBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todays_deal);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = getApplicationContext();

    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI () {
        iv_todaysdealBack = findViewById(R.id.iv_todaysdealBack);
        iv_todaysdealBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
