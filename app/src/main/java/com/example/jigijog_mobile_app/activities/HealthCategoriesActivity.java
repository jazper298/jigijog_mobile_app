package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.HealthProductAdapter;
import com.example.jigijog_mobile_app.adapters.HealthProductGridAdapter;
import com.example.jigijog_mobile_app.adapters.JewelryProductAdapter;
import com.example.jigijog_mobile_app.adapters.MobileProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Health;
import com.example.jigijog_mobile_app.models.Jewelry;
import com.example.jigijog_mobile_app.models.Mobile;
import com.example.jigijog_mobile_app.models.Women;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class HealthCategoriesActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private String message;

    private ImageView iv_healthcategoryBack, iv_healthcategoryGrid,iv_healthcategoryMore,iv_healthcategoryCart;
    private TextView tv_healthcategoryName;

    private ConstraintLayout emptyIndicator;


    private RecyclerView healthcategoryRecyclerView;
    private ArrayList<Health> healthArrayList;
    private HealthProductAdapter healthProductAdapter;
    private HealthProductGridAdapter healthProductGridAdapter;

    private Health selectedHealth = new Health();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();
        if(message.equals("Body Treatment")) {
            tv_healthcategoryName.setText( message);
            loadHealthBodyTreatment();
            iv_healthcategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHealthBodyTreatmentGrid();
                }
            });
        }else if (message.equals("Hair Salon")){
            tv_healthcategoryName.setText( message);
            loadHealthHairSalon();
            iv_healthcategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHealthHairSalonGrid();
                }
            });
        }else if (message.equals("Makeup/ lashes/ brows")) {
            tv_healthcategoryName.setText(message);
            loadHealthMakeup();
            iv_healthcategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHealthMakeupGrid();
                }
            });
        }else if (message.equals("Others")) {
            tv_healthcategoryName.setText(message);
            loadHealthOthers();
            iv_healthcategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHealthOthersGrid();
                }
            });
        }
        else if (message.equals("Bath & Body")) {
            tv_healthcategoryName.setText(message);
            loadHealthBath();
            iv_healthcategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHealthBathGrid();
                }
            });
        }
        else if (message.equals("Supplements")) {
            tv_healthcategoryName.setText(message);
            loadHealthSupplements();
            iv_healthcategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadHealthSupplementsGrid();
                }
            });
        }
    }



    private void initializeUI() {
        emptyIndicator = findViewById(R.id.view_Empty);
        tv_healthcategoryName = findViewById(R.id.tv_healthcategoryName);
        iv_healthcategoryGrid = findViewById(R.id.iv_healthcategoryGrid);
        iv_healthcategoryBack = findViewById(R.id.iv_healthcategoryBack);
        iv_healthcategoryCart = findViewById(R.id.iv_healthcategoryCart);
        iv_healthcategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_healthcategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_healthcategoryMore = findViewById(R.id.iv_healthcategoryMore);
        iv_healthcategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void loadHealthBodyTreatment() {
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllBodyTreatments", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductAdapter = new HealthProductAdapter(context, healthArrayList);
                        healthProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthHairSalon(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHairSalon", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductAdapter = new HealthProductAdapter(context, healthArrayList);
                        healthProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthMakeup(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/MakeupLashesBrows", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductAdapter = new HealthProductAdapter(context, healthArrayList);
                        healthProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthOthers(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHBPCOthers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductAdapter = new HealthProductAdapter(context, healthArrayList);
                        healthProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthBath(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllBathAndBody", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductAdapter = new HealthProductAdapter(context, healthArrayList);
                        healthProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthSupplements(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllSupplements", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductAdapter = new HealthProductAdapter(context, healthArrayList);
                        healthProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthBodyTreatmentGrid() {
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllBodyTreatments", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductGridAdapter = new HealthProductGridAdapter(context, healthArrayList);
                        healthProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthHairSalonGrid(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHairSalon", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductGridAdapter = new HealthProductGridAdapter(context, healthArrayList);
                        healthProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthMakeupGrid(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/MakeupLashesBrows", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductGridAdapter = new HealthProductGridAdapter(context, healthArrayList);
                        healthProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthOthersGrid(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllHBPCOthers", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductGridAdapter = new HealthProductGridAdapter(context, healthArrayList);
                        healthProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthBathGrid(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllBathAndBody", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductGridAdapter = new HealthProductGridAdapter(context, healthArrayList);
                        healthProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthSupplementsGrid(){
        healthcategoryRecyclerView = findViewById(R.id.healthcategoryRecyclerView);
        healthArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllSupplements", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Health healthModel = new Health();
                            healthModel.setId(id);
                            healthModel.setName(name);
                            healthModel.setAdded_by(added_by);
                            healthModel.setCategory_id(category_id);
                            healthModel.setSubcategory_id(subcategory_id);
                            healthModel.setSubsubcategory_id(subsubcategory_id);
                            healthModel.setBrand_id(brand_id);
                            healthModel.setPhotos(photos);
                            healthModel.setThumbnail_img(thumbnail_img);
                            healthModel.setFeatured_img(featured_img);
                            healthModel.setFlash_deal_img(flash_deal_img);
                            healthModel.setVideo_provider(video_provider);
                            healthModel.setVideo_link(video_link);
                            healthModel.setTags(tags);
                            healthModel.setDescription(description);
                            healthModel.setUnit_price(unit_price);
                            healthModel.setPurchase_price(purchase_price);
                            healthModel.setChoice_options(choice_options);
                            healthModel.setColors(colors);
                            healthModel.setVariations(variations);
                            healthModel.setTodays_deal(todays_deal);
                            healthModel.setPublished(published);
                            healthModel.setFeatured(featured);
                            healthModel.setCurrent_stock(current_stock);
                            healthModel.setUnit(unit);
                            healthModel.setDiscount(discount);
                            healthModel.setDiscount_type(discount_type);
                            healthModel.setTax(tax);
                            healthModel.setTax_type(tax_type);
                            healthModel.setShipping_type(shipping_type);
                            healthModel.setShipping_cost(shipping_cost);
                            healthModel.setWeight(weight);
                            healthModel.setParcel_size(parcel_size);
                            healthModel.setNum_of_sale(num_of_sale);
                            healthModel.setMeta_title(meta_title);
                            healthModel.setMeta_description(meta_description);
                            healthModel.setMeta_img(meta_img);
                            healthModel.setPdf(pdf);
                            healthModel.setSlug(slug);
                            healthModel.setRating(rating);
                            healthModel.setCreated_at(created_at);
                            healthModel.setUpdated_at(updated_at);

                            healthArrayList.add(healthModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        healthcategoryRecyclerView.setLayoutManager(layoutManager);

                        healthProductGridAdapter = new HealthProductGridAdapter(context, healthArrayList);
                        healthProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedHealth = healthArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedHealth.getName());
                                intent.putExtra("ITEMPRICE", selectedHealth.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedHealth.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedHealth.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedHealth.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        healthcategoryRecyclerView.setAdapter(healthProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
}
