package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.models.UserCustomer;
import com.example.jigijog_mobile_app.utils.Tools;
import com.example.jigijog_mobile_app.utils.UserRole;

import es.dmoral.toasty.Toasty;

public class SettingsActivity extends AppCompatActivity {

    private View view;
    private Context context;

    private ImageView iv_settingsBack, iv_settingsCountry;
    private TextView tv_accountInfo, tv_accountAddress, tv_accountMessages, tv_accountPolicies, tv_accountHelp,tv_accountLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI() {
        iv_settingsBack = findViewById(R.id.iv_settingsBack);
        iv_settingsCountry = findViewById(R.id.iv_settingsCountry);
        tv_accountInfo = findViewById(R.id.tv_accountInfo);
        tv_accountAddress = findViewById(R.id.tv_accountAddress);
        tv_accountMessages = findViewById(R.id.tv_accountMessages);
        tv_accountPolicies = findViewById(R.id.tv_accountPolicies);
        tv_accountHelp = findViewById(R.id.tv_accountHelp);
        tv_accountLogout = findViewById(R.id.tv_accountLogout);
        iv_settingsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_settingsCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Country Settings").show();
            }
        });
        tv_accountInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Account Info Settings").show();
            }
        });
        tv_accountAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Account Address Settings").show();
            }
        });
        tv_accountMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Account Messages Settings").show();
            }
        });
        tv_accountPolicies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Account Policies Settings").show();
            }
        });
        tv_accountHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Account Help Settings").show();
            }
        });
        tv_accountLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialogLogout();
            }
        });
    }
    private void openDialogLogout() {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Logout")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logoutUser();
                        finish();
                    }
                })
                .setNegativeButton("NO", null)
                .create();
        dialog.show();

    }

    private void logoutUser(){
        UserCustomer.clearSession(context);
        UserRole.clearRole(context);
        MainActivity mainActivity = new MainActivity();
        mainActivity.checkCustomerSession();
    }
}
