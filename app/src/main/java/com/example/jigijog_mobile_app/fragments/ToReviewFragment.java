package com.example.jigijog_mobile_app.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jigijog_mobile_app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ToReviewFragment extends Fragment {
    private View view;
    private Context context;

    public ToReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_to_review, container, false);
        return view;
    }

}
