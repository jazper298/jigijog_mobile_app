package com.example.jigijog_mobile_app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.activities.ChatActivity;
import com.example.jigijog_mobile_app.adapters.MessagesAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Messages;
import com.example.jigijog_mobile_app.utils.Tools;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class MessagesFragment extends Fragment {

    public static final int DIALOG_QUEST_CODE = 300;
    private View view;
    private Context context;

    private ImageView iv_chats, iv_notifications;

    private RecyclerView messagesRecyclerView;
    private ArrayList<Messages>  messagesArrayList;
    private MessagesAdapter messagesAdapter;
    public MessagesFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_messages, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Tools.setSystemBarColor(getActivity(), R.color.deep_orange_400);
        Tools.setSystemBarLight(getActivity());
        initializeUI();
        loadAllMessages();
    }

    private void initializeUI () {
        iv_chats = view.findViewById(R.id.iv_chats);
        iv_notifications = view.findViewById(R.id.iv_notifications);
        iv_chats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity.class);
                startActivity(intent);
            }
        });
        iv_notifications.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity.class);
                startActivity(intent);
            }
        });

    }

    private void loadAllMessages() {
        messagesRecyclerView = view.findViewById(R.id.messagesRecyclerView);
        messagesArrayList = new ArrayList<>();

        messagesArrayList.add(new Messages(R.drawable.ic_stars_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider1,"Guess the Price, Win the Price from 6-10PM today! \n Click to learn more."));
        messagesArrayList.add(new Messages(R.drawable.ic_stars_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider2,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        messagesArrayList.add(new Messages(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider4,"Guess the Price, Win the Price from 6-10PM today! \nClick to learn more."));
        messagesArrayList.add(new Messages(R.drawable.ic_check_box_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider7,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        messagesRecyclerView.setLayoutManager(layoutManager);

        messagesAdapter = new MessagesAdapter(context, messagesArrayList);
        messagesAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        messagesRecyclerView.setAdapter(messagesAdapter);
    }

}
