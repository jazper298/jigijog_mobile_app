package com.example.jigijog_mobile_app.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.HttpProvider;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.activities.AssortedProductsActivity;
import com.example.jigijog_mobile_app.activities.CategoriesActivity;
import com.example.jigijog_mobile_app.activities.FeaturedProductsActivity;
import com.example.jigijog_mobile_app.activities.ProductViewActivity;
import com.example.jigijog_mobile_app.activities.TodaysDealActivity;
import com.example.jigijog_mobile_app.adapters.AssortedAdapter;
import com.example.jigijog_mobile_app.adapters.CategoriesAdapter;
import com.example.jigijog_mobile_app.adapters.CustomSwipeAdapter;
import com.example.jigijog_mobile_app.adapters.FeaturedAdapter;
import com.example.jigijog_mobile_app.adapters.TodaysDealAdapter;
import com.example.jigijog_mobile_app.adapters.ViewPagerAdapter;
import com.example.jigijog_mobile_app.data.DataGenerator;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Assorted;
import com.example.jigijog_mobile_app.models.Categories;
import com.example.jigijog_mobile_app.models.Featured;
import com.example.jigijog_mobile_app.models.FlashDeal;
import com.example.jigijog_mobile_app.models.Flipper;
import com.example.jigijog_mobile_app.models.Slide;
import com.example.jigijog_mobile_app.models.Todaysdeal;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;


public class HomeFragment extends Fragment {

    private View view;
    private Context context;

    private ViewFlipper viewFlipper;
    private RecyclerView categoryRecyclerview;
    private RecyclerView todaysdealRecyclerView;
    private RecyclerView featuredRecyclerView;
    private RecyclerView assortedRecyclerview;
    private ArrayList<Categories> categoriesArrayList;
    private ArrayList<Todaysdeal> todaysdealArrayList;
    private ArrayList<Featured> featuredArrayList;
    private List<Assorted> assortedArrayList;
    private CategoriesAdapter categoriesAdapter;
    private TodaysDealAdapter todaysDealAdapter;
    private FeaturedAdapter featuredAdapter;
    private AssortedAdapter assortedAdapter;


    private Categories selectedCategories = new Categories();
    private Assorted selectedAssorted = new Assorted();
    private Featured selectedFeatured = new Featured();
    private Todaysdeal selectedTodaysdeal = new Todaysdeal();

    private int item_per_display = 10;

    private TextView todaysDealSee,featureProductsSee,assortedSee;

    private SwipeRefreshLayout swipeRefreshLayout2;
    private SwipeRefreshLayout swipeRefreshLayout3;
    private SwipeRefreshLayout swipeRefreshLayout4;
    private SwipeRefreshLayout swipeRefreshLayout5;

    private ViewPager viewPager;

    private ArrayList<Flipper> flipperArrayList;
    private Flipper flipperModel;


    private CustomSwipeAdapter customSwipeAdapter;
    private ArrayList<Slide> slideArrayList;
    private LinearLayout dotsLayout;
    private int custom_pos = 0;

    private Timer timer;
    private int current_pos = 0;

    private ArrayList<FlashDeal> flashDealArrayList;

    private ImageView iv_mensHotImage, iv_mensHotImage1,iv_mensHotImage3, iv_mensHotImage4, iv_mensHotImage5;
    private TextView tv_mensHotItemName, tv_mensHotItemName1,tv_mensHotItemName3,tv_mensHotItemName4,tv_mensHotItemName5;
    private TextView tv_mensHotPrice, tv_mensHotPrice1,tv_mensHotPrice3,tv_mensHotPrice4,tv_mensHotPrice5;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getContext();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Tools.setSystemBarColor(getActivity(), R.color.grey_5);
        Tools.setSystemBarLight(getActivity());

        initializeUI();
        loadSliderImages();
        loadCategories();
        loadFlashDeal();
        loadTodaysDeal();
        loadFeaturedProducts();
        loadAssortedProducts();
    }

    private void initializeUI() {
        iv_mensHotImage = view.findViewById(R.id.iv_mensHotImage);
        iv_mensHotImage1 = view.findViewById(R.id.iv_mensHotImage1);
        iv_mensHotImage3 = view.findViewById(R.id.iv_mensHotImage3);
        iv_mensHotImage4 = view.findViewById(R.id.iv_mensHotImage4);
        iv_mensHotImage5 = view.findViewById(R.id.iv_mensHotImage5);

        tv_mensHotItemName = view.findViewById(R.id.tv_mensHotItemName);
        tv_mensHotItemName1 = view.findViewById(R.id.tv_mensHotItemName1);
        tv_mensHotItemName3 = view.findViewById(R.id.tv_mensHotItemName3);
        tv_mensHotItemName4 = view.findViewById(R.id.tv_mensHotItemName4);
        tv_mensHotItemName5 = view.findViewById(R.id.tv_mensHotItemName5);

        tv_mensHotPrice = view.findViewById(R.id.tv_mensHotPrice);
        tv_mensHotPrice1 = view.findViewById(R.id.tv_mensHotPrice1);
        tv_mensHotPrice3 = view.findViewById(R.id.tv_mensHotPrice3);
        tv_mensHotPrice4 = view.findViewById(R.id.tv_mensHotPrice4);
        tv_mensHotPrice5 = view.findViewById(R.id.tv_mensHotPrice5);

        todaysDealSee = view.findViewById(R.id.todaysDealSee);
        featureProductsSee = view.findViewById(R.id.featureProductsSee);
        swipeRefreshLayout2 = view.findViewById(R.id.swipe_Home2);
        swipeRefreshLayout3 = view.findViewById(R.id.swipe_Home3);
        swipeRefreshLayout4 = view.findViewById(R.id.swipe_Home4);
        swipeRefreshLayout5 = view.findViewById(R.id.swipe_Home5);

        assortedSee = view.findViewById(R.id.assortedSee);

        swipeRefreshLayout2.setRefreshing(false);
        swipeRefreshLayout3.setRefreshing(false);
        swipeRefreshLayout4.setRefreshing(false);
        todaysDealSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TodaysDealActivity.class);
                startActivity(intent);
            }
        });
        featureProductsSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FeaturedProductsActivity.class);
                startActivity(intent);
            }
        });
        assortedSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AssortedProductsActivity.class);
                startActivity(intent);
            }
        });

    }

    private void loadSliderImages(){
        viewPager = view.findViewById(R.id.view_pager);
        dotsLayout = view.findViewById(R.id.dotsContainer);
        slideArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getSliderImages", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String photos = jsonObject.getString("photo");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");

                        Slide slideModel = new Slide();
                        slideModel.setId(id);
                        slideModel.setPhotos(photos);
                        slideModel.setCreated_at(created_at);
                        slideModel.setUpdated_at(updated_at);

                        slideArrayList.add(slideModel);
                    }
                    customSwipeAdapter = new CustomSwipeAdapter(context, slideArrayList);
                    viewPager.setAdapter(customSwipeAdapter);
                    loadPreparedDots(custom_pos++);
                    loadCreateSlideShow();
                    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            if (custom_pos>slideArrayList.size()){
                                custom_pos = 0;
                            }
                            loadPreparedDots(custom_pos++);
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadCreateSlideShow(){
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (current_pos == Integer.MAX_VALUE) {
                    current_pos = 0;
                }
                viewPager.setCurrentItem(current_pos++, true);
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);

            }
        },250, 2500);

    }
    private void loadPreparedDots(int currentSlidePos){
        if(dotsLayout.getChildCount()>0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[slideArrayList.size()];
        for (int i =0; i<slideArrayList.size();i++){
            dots[i] = new ImageView(context);
            if(i==currentSlidePos){
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.active_dot));
            }else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.inactive_dots));
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(4, 0, 4,0);
            dotsLayout.addView(dots[i],layoutParams);
        }
    }

    private void loadCategories() {
        categoryRecyclerview = view.findViewById(R.id.categoriesRecyclerview);
        categoriesArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCategories", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String banner = jsonObject.getString("banner");

                        Categories categories = new Categories();
                        categories.setId(id);
                        categories.setName(name);
                        categories.setBanner(banner);

                        categoriesArrayList.add(categories);
                    }
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    categoryRecyclerview.setLayoutManager(layoutManager);

                    categoriesAdapter = new CategoriesAdapter(context, categoriesArrayList);
                    categoriesAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedCategories = categoriesArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, CategoriesActivity.class);
                            intent.putExtra("CATEGORYID", selectedCategories.getId());
                            intent.putExtra("CATEGORYNAME", selectedCategories.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    categoryRecyclerview.setAdapter(categoriesAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }


    private void loadFlashDeal() {
        flashDealArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllFlashDeal", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("CATEGORIES: " + Arrays.toString(response));
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String flash_deal_id = jsonObject.getString("flash_deal_id");
                        String product_id = jsonObject.getString("product_id");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");

                        FlashDeal flashDeal = new FlashDeal();
                        flashDeal.setId(id);
                        flashDeal.setFlash_deal_id(flash_deal_id);
                        flashDeal.setProduct_id(product_id);
                        flashDeal.setDiscount(discount);
                        flashDeal.setDiscount_type(discount_type);
                        flashDeal.setCreated_at(created_at);
                        flashDeal.setUpdated_at(updated_at);

                        flashDealArrayList.add(flashDeal);
                    }
                    Iterator<FlashDeal> itr = flashDealArrayList.iterator();
                    while (itr.hasNext()) {
                        FlashDeal element = itr.next();
                        //tv_mensHotItemName.setText(element.g);
                    }
                    if(!flashDealArrayList.isEmpty()){
//                        tv_mensHotItemName.setText( flashDealArrayList.get(0));
//                        tv_mensHotItemName1.setText(flashDealArrayList.get(1));
//                        tv_mensHotItemName3.setText( flashDealArrayList.get(2));
//                        tv_mensHotItemName4.setText(flashDealArrayList.get(3));
//                        tv_mensHotItemName5.setText( flashDealArrayList.get(4));
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadTodaysDeal() {
        todaysdealRecyclerView = view.findViewById(R.id.todaysDealRecyclerview);
        todaysdealArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getTodaysDeal", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("CATEGORIES: " + Arrays.toString(response));
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String photos = jsonObject.getString("flash_deal_img");
                        String unit_price = jsonObject.getString("unit_price");
                        String discount = jsonObject.getString("discount");

                        Todaysdeal todaysdeal = new Todaysdeal();
                        todaysdeal.setId(id);
                        todaysdeal.setName(name);
                        todaysdeal.setPhotos(photos);
                        todaysdeal.setUnit_price(unit_price);
                        todaysdeal.setDiscount(discount);

                        todaysdealArrayList.add(todaysdeal);
                    }
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    todaysdealRecyclerView.setLayoutManager(layoutManager);

                    todaysDealAdapter = new TodaysDealAdapter(context, todaysdealArrayList);
                    todaysDealAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {

                        }
                    });
                    todaysdealRecyclerView.setAdapter(todaysDealAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadFeaturedProducts() {
        featuredRecyclerView = view.findViewById(R.id.featuredRecyclerview);
        featuredArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getFeaturedProducts", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Featured featuredModel = new Featured();
                        featuredModel.setId(id);
                        featuredModel.setName(name);
                        featuredModel.setAdded_by(added_by);
                        featuredModel.setCategory_id(category_id);
                        featuredModel.setSubcategory_id(subcategory_id);
                        featuredModel.setSubsubcategory_id(subsubcategory_id);
                        featuredModel.setBrand_id(brand_id);
                        featuredModel.setPhotos(photos);
                        featuredModel.setThumbnail_img(thumbnail_img);
                        featuredModel.setFeatured_img(featured_img);
                        featuredModel.setFlash_deal_img(flash_deal_img);
                        featuredModel.setVideo_provider(video_provider);
                        featuredModel.setVideo_link(video_link);
                        featuredModel.setTags(tags);
                        featuredModel.setDescription(description);
                        featuredModel.setUnit_price(unit_price);
                        featuredModel.setPurchase_price(purchase_price);
                        featuredModel.setChoice_options(choice_options);
                        featuredModel.setColors(colors);
                        featuredModel.setVariations(variations);
                        featuredModel.setTodays_deal(todays_deal);
                        featuredModel.setPublished(published);
                        featuredModel.setFeatured(featured);
                        featuredModel.setCurrent_stock(current_stock);
                        featuredModel.setUnit(unit);
                        featuredModel.setDiscount(discount);
                        featuredModel.setDiscount_type(discount_type);
                        featuredModel.setTax(tax);
                        featuredModel.setTax_type(tax_type);
                        featuredModel.setShipping_type(shipping_type);
                        featuredModel.setShipping_cost(shipping_cost);
                        featuredModel.setWeight(weight);
                        featuredModel.setParcel_size(parcel_size);
                        featuredModel.setNum_of_sale(num_of_sale);
                        featuredModel.setMeta_title(meta_title);
                        featuredModel.setMeta_description(meta_description);
                        featuredModel.setMeta_img(meta_img);
                        featuredModel.setPdf(pdf);
                        featuredModel.setSlug(slug);
                        featuredModel.setRating(rating);
                        featuredModel.setCreated_at(created_at);
                        featuredModel.setUpdated_at(updated_at);

                        featuredArrayList.add(featuredModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,  2, GridLayoutManager.HORIZONTAL, false);
                    featuredRecyclerView.setLayoutManager(layoutManager);

                    featuredAdapter = new FeaturedAdapter(context, featuredArrayList);
                    featuredAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedFeatured = featuredArrayList.get(position);

//                            Bundle extras = new Bundle();
//                            Intent intent = new Intent(context, ProductViewActivity.class);
//                            intent.putExtra("ITEMNAME", selectedFeatured.getName());
//                            intent.putExtra("ITEMPRICE", selectedFeatured.getUnit_price());
//                            intent.putExtra("ITEMDISPRICE", selectedFeatured.getDiscount());
//                            intent.putExtra("ITEMIMAGE",selectedFeatured.getFeatured_img());
//                            intent.putExtras(extras);
//                            startActivity(intent);
                        }
                    });
                    featuredRecyclerView.setAdapter(featuredAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadAssortedProducts() {
        assortedRecyclerview = view.findViewById(R.id.assortedRecyclerView);
        assortedArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAssortedItems", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Assorted assortedModel = new Assorted();
                        assortedModel.setId(id);
                        assortedModel.setName(name);
                        assortedModel.setAdded_by(added_by);
                        assortedModel.setCategory_id(category_id);
                        assortedModel.setSubcategory_id(subcategory_id);
                        assortedModel.setSubsubcategory_id(subsubcategory_id);
                        assortedModel.setBrand_id(brand_id);
                        assortedModel.setPhotos(photos);
                        assortedModel.setThumbnail_img(thumbnail_img);
                        assortedModel.setFeatured_img(featured_img);
                        assortedModel.setFlash_deal_img(flash_deal_img);
                        assortedModel.setVideo_provider(video_provider);
                        assortedModel.setVideo_link(video_link);
                        assortedModel.setTags(tags);
                        assortedModel.setDescription(description);
                        assortedModel.setUnit_price(unit_price);
                        assortedModel.setPurchase_price(purchase_price);
                        assortedModel.setChoice_options(choice_options);
                        assortedModel.setColors(colors);
                        assortedModel.setVariations(variations);
                        assortedModel.setTodays_deal(todays_deal);
                        assortedModel.setPublished(published);
                        assortedModel.setFeatured(featured);
                        assortedModel.setCurrent_stock(current_stock);
                        assortedModel.setUnit(unit);
                        assortedModel.setDiscount(discount);
                        assortedModel.setDiscount_type(discount_type);
                        assortedModel.setTax(tax);
                        assortedModel.setTax_type(tax_type);
                        assortedModel.setShipping_type(shipping_type);
                        assortedModel.setShipping_cost(shipping_cost);
                        assortedModel.setWeight(weight);
                        assortedModel.setParcel_size(parcel_size);
                        assortedModel.setNum_of_sale(num_of_sale);
                        assortedModel.setMeta_title(meta_title);
                        assortedModel.setMeta_description(meta_description);
                        assortedModel.setMeta_img(meta_img);
                        assortedModel.setPdf(pdf);
                        assortedModel.setSlug(slug);
                        assortedModel.setRating(rating);
                        assortedModel.setCreated_at(created_at);
                        assortedModel.setUpdated_at(updated_at);

                        assortedArrayList.add(assortedModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,  2, GridLayoutManager.VERTICAL, false);
                    assortedRecyclerview.setLayoutManager(layoutManager);

                    assortedAdapter = new AssortedAdapter(context, assortedArrayList);
                    assortedAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedAssorted = assortedArrayList.get(position);

//                            Bundle extras = new Bundle();
//                            Intent intent = new Intent(context, ProductViewActivity.class);
//                            intent.putExtra("ITEMNAME", selectedFeatured.getName());
//                            intent.putExtra("ITEMPRICE", selectedFeatured.getUnit_price());
//                            intent.putExtra("ITEMDISPRICE", selectedFeatured.getDiscount());
//                            intent.putExtra("ITEMIMAGE",selectedFeatured.getFeatured_img());
//                            intent.putExtras(extras);
//                            startActivity(intent);
                        }
                    });
                    assortedRecyclerview.setAdapter(assortedAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });


    }

}
