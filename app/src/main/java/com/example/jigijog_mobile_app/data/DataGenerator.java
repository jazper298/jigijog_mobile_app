package com.example.jigijog_mobile_app.data;

import android.content.Context;
import android.content.res.TypedArray;


import com.example.jigijog_mobile_app.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

@SuppressWarnings("ResourceType")
public class DataGenerator {

    private static Random r = new Random();

    public static int randInt(int max) {
        int min = 0;
        return r.nextInt((max - min) + 1) + min;
    }

    public static List<Integer> getAssortedImages(Context ctx) {
        List<Integer> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.sample_images);
        for (int i = 0; i < drw_arr.length(); i++) {
            items.add(drw_arr.getResourceId(i, -1));
        }
        //Collections.shuffle(items);
        return items;
    }
    public static List<String> getAssortedName(Context ctx) {
        List<String> items = new ArrayList<>();
        String name_arr[] = ctx.getResources().getStringArray(R.array.sample_images_name);
        for (String s : name_arr) items.add(s);
       // Collections.shuffle(items);
        return items;
    }
    public static List<String> getAssortedPrice(Context ctx) {
        List<String> items = new ArrayList<>();
        String name_arr[] = ctx.getResources().getStringArray(R.array.sample_price);
        for (String s : name_arr) items.add(s);
        //Collections.shuffle(items);
        return items;
    }
    public static List<String> getAssortedPriceDis(Context ctx) {
        List<String> items = new ArrayList<>();
        String name_arr[] = ctx.getResources().getStringArray(R.array.sample_price_dis);
        for (String s : name_arr) items.add(s);
       // Collections.shuffle(items);
        return items;
    }

}
